package cz.muni.pa165.facade;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.generated.model.ApplicationCreateDto;
import cz.muni.pa165.generated.model.ApplicationDto;
import cz.muni.pa165.mappers.ApplicationMapper;
import cz.muni.pa165.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Michal Badin
 */
@Service
@Transactional
public class ApplicationFacade {
    private final ApplicationService applicationService;
    private final ApplicationMapper applicationMapper;

    @Autowired
    public ApplicationFacade(ApplicationService applicationService, ApplicationMapper applicationMapper) {
        this.applicationService = applicationService;
        this.applicationMapper = applicationMapper;
    }

    @Transactional(readOnly = true)
    public List<ApplicationDto> getAll() {
        return applicationMapper.mapToList(applicationService.findAll());
    }

    public ApplicationDto create(ApplicationCreateDto applicationCreateDto) {
        return applicationMapper.mapToDto(applicationService.create(applicationMapper.mapFromCreateDto(applicationCreateDto)));
    }

    @Cacheable(cacheNames = "applications", key = "#id")
    @Transactional(readOnly = true)
    public ApplicationDto findById(Long id) {
        return applicationMapper.mapToDto(applicationService.findById(id));
    }

    public ApplicationDto setStatus(Long id, ApplicationStatusEnum applicationStatusEnum) {
        return applicationMapper.mapToDto(applicationService.setStatus(id, applicationStatusEnum));
    }
}
