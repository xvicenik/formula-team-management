package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Michal Badin
 */
@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
}
