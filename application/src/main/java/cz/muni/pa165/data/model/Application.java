package cz.muni.pa165.data.model;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Michal Badin
 */
@Entity
@Table(name = "application")
public class Application implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ApplicationStatusEnum status;

    @NotEmpty
    @Size(max = 35)
    private String name;

    @NotEmpty
    @Size(max = 35)
    private String surname;

    @Past
    @NotNull
    private LocalDate birthday;

    @Email
    private String email;

    @Column(name = "filling_out_date")
    @PastOrPresent
    private LocalDate fillingOutDate;

    public Application() {
    }

    public Application(ApplicationStatusEnum status, String name, String surname, LocalDate birthday, String email, LocalDate fillingOutDate) {
        this.status = status;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.email = email;
        this.fillingOutDate = fillingOutDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApplicationStatusEnum status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getFillingOutDate() {
        return fillingOutDate;
    }

    public void setFillingOutDate(LocalDate fillingOutDate) {
        this.fillingOutDate = fillingOutDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Application application)) {
            return false;
        }
        return Objects.equals(getName(), application.getName()) &&
                Objects.equals(getSurname(), application.getSurname()) &&
                Objects.equals(getBirthday(), application.getBirthday());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getBirthday());
    }
}
