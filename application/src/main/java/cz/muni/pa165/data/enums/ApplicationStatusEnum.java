package cz.muni.pa165.data.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Michal Badin
 */
public enum ApplicationStatusEnum {
    PENDING("pending"),

    REJECTED("rejected"),

    ACCEPTED("accepted");

    private final String value;

    ApplicationStatusEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ApplicationStatusEnum fromValue(String value) {
        for (ApplicationStatusEnum s : ApplicationStatusEnum.values()) {
            if (s.value.equals(value)) {
                return s;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}