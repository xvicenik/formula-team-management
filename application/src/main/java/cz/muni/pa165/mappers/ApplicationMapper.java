package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.Application;
import cz.muni.pa165.generated.model.ApplicationCreateDto;
import cz.muni.pa165.generated.model.ApplicationDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Michal Badin
 */
@Mapper(componentModel = "spring")
public interface ApplicationMapper {
    ApplicationDto mapToDto(Application application);

    Application mapFromCreateDto(ApplicationCreateDto carCreateDto);

    List<ApplicationDto> mapToList(List<Application> cars);
}

