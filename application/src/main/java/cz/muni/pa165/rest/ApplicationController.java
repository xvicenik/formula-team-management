package cz.muni.pa165.rest;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.facade.ApplicationFacade;
import cz.muni.pa165.generated.api.ApplicationServiceApiDelegate;
import cz.muni.pa165.generated.model.ApplicationCreateDto;
import cz.muni.pa165.generated.model.ApplicationDto;
import cz.muni.pa165.generated.model.ApplicationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Michal Badin
 */

@Component
public class ApplicationController implements ApplicationServiceApiDelegate {

    private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);
    private final ApplicationFacade applicationFacade;

    @Autowired
    public ApplicationController(ApplicationFacade applicationFacade) {
        this.applicationFacade = applicationFacade;
    }

    @Override
    public ResponseEntity<List<ApplicationDto>> getAllApplications() {
        log.debug("getAllApplications() called");
        List<ApplicationDto> applications = applicationFacade.getAll();
        return new ResponseEntity<>(applications, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApplicationDto> createApplication(ApplicationCreateDto applicationCreateDto) {
        log.debug("createApplication() called");
        ApplicationDto applicationDto = applicationFacade.create(applicationCreateDto);
        return new ResponseEntity<>(applicationDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApplicationDto> getApplication(Long id) {
        log.debug("getApplication() called");
        ApplicationDto applicationDto = applicationFacade.findById(id);
        return new ResponseEntity<>(applicationDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApplicationDto> setStatus(Long id, ApplicationStatus applicationStatus) {
        log.debug("setStatus() called");
        ApplicationDto applicationDto = applicationFacade.setStatus(id, ApplicationStatusEnum.fromValue(applicationStatus.getValue()));
        return new ResponseEntity<>(applicationDto, HttpStatus.OK);
    }
}
