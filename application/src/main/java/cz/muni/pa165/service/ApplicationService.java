package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.data.model.Application;
import cz.muni.pa165.data.repository.ApplicationRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.time.LocalDate;
import java.util.*;

/**
 * @author Michal Badin
 */
@Service
public class ApplicationService {
    private static final Logger log = LoggerFactory.getLogger(ApplicationService.class);
    private static final String NOTIFICATION_MODULE_DOCKER = "http://notification:8083";
    private static final String NOTIFICATION_MODULE_LOCALHOST = "http://localhost:8083";
    private static final String NOTIFICATION_MODULE_URI_NEW = "/notification/application/new";
    private static final String NOTIFICATION_MODULE_URI_STATUS = "/notification/application/status";
    @Value("${notification.receivers}")
    private final List<String> notificationReceivers = new ArrayList<>();
    private final ApplicationRepository applicationRepository;
    private final WebClient webClient;
    private String NOTIFICATION_MODULE_URL;

    @Autowired
    public ApplicationService(ApplicationRepository applicationRepository, WebClient.Builder webClientBuilder) {
        this.applicationRepository = applicationRepository;

        //if running in docker, modules cannot communicate through localhost
        if (System.getenv("DOCKER") != null) {
            NOTIFICATION_MODULE_URL = NOTIFICATION_MODULE_DOCKER;
        } else {
            NOTIFICATION_MODULE_URL = NOTIFICATION_MODULE_LOCALHOST;
        }
        this.webClient = webClientBuilder.baseUrl(NOTIFICATION_MODULE_URL).build();
    }

    @Transactional(readOnly = true)
    public List<Application> findAll() {
        return applicationRepository.findAll();
    }

    public Application create(Application application) {
        application.setFillingOutDate(LocalDate.now());
        application.setStatus(ApplicationStatusEnum.PENDING);

        var savedApplication = applicationRepository.save(application);

        try {
            sendPostRequest(savedApplication, NOTIFICATION_MODULE_URI_NEW, notificationReceivers);
        } catch (WebClientException e) {
            log.debug(String.format("The notification module is not reachable on the URL: %s, exception %s", NOTIFICATION_MODULE_URL + NOTIFICATION_MODULE_URI_NEW, e));
        }

        return savedApplication;
    }

    @Transactional(readOnly = true)
    public Application findById(Long id) {
        return applicationRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(String.format("%s with id %s was not found", Application.class, id)));
    }

    public Application setStatus(Long id, ApplicationStatusEnum applicationStatusEnum) {
        Optional<Application> applicationFromDb = applicationRepository.findById(id);
        if (applicationFromDb.isEmpty()) {
            throw new ResourceNotFoundException(String.format("%s with id %s was not found", Application.class, id));
        }

        applicationFromDb.get().setStatus(applicationStatusEnum);

        var savedApplication = applicationRepository.save(applicationFromDb.get());

        if (savedApplication.getStatus() != ApplicationStatusEnum.PENDING) {
            try {
                sendPostRequest(savedApplication, NOTIFICATION_MODULE_URI_STATUS, List.of(savedApplication.getEmail()));
            } catch (WebClientException e) {
                log.debug(String.format("The notification module is not reachable on the URL: %s, exception %s", NOTIFICATION_MODULE_URL + NOTIFICATION_MODULE_URI_STATUS, e));
            }
        }

        return savedApplication;
    }

    public void sendPostRequest(Application application, String uri, List<String> receivers) throws RestClientException {
        Map<String, Object> notification = new HashMap<>();
        notification.put("application", application);
        notification.put("receivers", receivers);

        webClient.post()
                .uri(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(notification)
                .retrieve()
                .bodyToMono(Resource.class)
                .block();
    }
}
