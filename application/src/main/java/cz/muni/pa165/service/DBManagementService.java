package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.data.model.Application;
import cz.muni.pa165.data.repository.ApplicationRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

/**
 * @author Michal Badin
 */
@Service
@Profile("dev")
public class DBManagementService {
    private final ApplicationRepository applicationRepository;

    @Autowired
    public DBManagementService(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Transactional
    @PostConstruct
    public void seed() {
        saveApplication(ApplicationStatusEnum.PENDING,
                "Marek",
                "Mrkvicka",
                "marek.mrkvicka@example.com",
                LocalDate.of(1999, 3, 8),
                LocalDate.now());
        saveApplication(ApplicationStatusEnum.ACCEPTED,
                "Anuja",
                "Ankska",
                "ankska.anuja@example.com",
                LocalDate.of(2000, 1, 30),
                LocalDate.of(2010, 7, 18));
        saveApplication(ApplicationStatusEnum.ACCEPTED,
                "Xiao",
                "Chen",
                "xiao.chen@example.com",
                LocalDate.of(1995, 7, 29),
                LocalDate.of(2012, 10, 9));
        saveApplication(ApplicationStatusEnum.REJECTED,
                "Gertruda",
                "Fialkova",
                "gertruda.fialkova@example.com",
                LocalDate.of(1965, 6, 2),
                LocalDate.of(2023, 4, 26));
    }

    private void saveApplication(ApplicationStatusEnum status, String name, String surname, String email, LocalDate birthday, LocalDate fillingOutDate) {
        Application application = new Application(status, name, surname, birthday, email, fillingOutDate);
        applicationRepository.save(application);
    }

    @Transactional
    public void clear() {
        applicationRepository.deleteAll();
    }
}
