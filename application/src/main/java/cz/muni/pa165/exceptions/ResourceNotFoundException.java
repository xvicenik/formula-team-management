package cz.muni.pa165.exceptions;

/**
 * @author Michal Badin
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}