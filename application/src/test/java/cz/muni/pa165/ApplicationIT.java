package cz.muni.pa165;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "mvn verify / mvn test / mvn clean install".
 * No need to test all the rest controllers logic here, just test that the endpoints are working
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ApplicationIT {

    private static final Logger log = LoggerFactory.getLogger(ApplicationIT.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testCreateApplication() throws Exception {
        log.info("testApplicationModuleResponse() running");

        String testRequestContent = "{\"name\":\"John\",\"surname\":\"Doe\",\"birthday\":\"2000-01-01\",\"email\":\"john.doe@aaa.com\"}";

        String response = mockMvc.perform(
                        post("/application")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(testRequestContent))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("response: {}", response);

        String expectedResponse = "{\"id\":1,\"name\":\"John\",\"surname\":\"Doe\",\"birthday\":\"2000-01-01\",\"email\":\"john.doe@aaa.com\",\"fillingOutDate\":\"" + LocalDate.now() + "\",\"status\":\"pending\"}";

        assertEquals(expectedResponse, response);
    }
}