package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.data.model.Application;
import cz.muni.pa165.data.repository.ApplicationRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class ApplicationServiceTest {
    ApplicationRepository applicationRepository = Mockito.mock(ApplicationRepository.class);
    WebClient.Builder webClientBuilder = WebClient.builder();
    ApplicationService service = new ApplicationService(applicationRepository, webClientBuilder);
    Application application = new Application(ApplicationStatusEnum.ACCEPTED, "John", "Doe",
            LocalDate.now().minusYears(20), "john.doe@example.com", LocalDate.now());


    @Test
    void create() {
        when(applicationRepository.save(application)).thenReturn(application);

        Application created = service.create(application);

        assertEquals(application, created);
        assertEquals(ApplicationStatusEnum.PENDING, created.getStatus());
    }

    @Test
    void setStatus() {
        Application expected = new Application(ApplicationStatusEnum.REJECTED, "John", "Doe",
                LocalDate.now().minusYears(20), "john.doe@example.com", LocalDate.now());
        when(applicationRepository.save(application)).thenReturn(expected);
        when(applicationRepository.findById(application.getId())).thenReturn(java.util.Optional.ofNullable(application));

        Application actual = service.setStatus(application.getId(), ApplicationStatusEnum.REJECTED);

        assertEquals(expected, actual);
    }

    @Test
    void setStatusUnknownApplication() {
        when(applicationRepository.findById(application.getId())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> service.setStatus(application.getId(), ApplicationStatusEnum.REJECTED));
    }
}