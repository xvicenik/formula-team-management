package cz.muni.pa165.rest;

import cz.muni.pa165.data.enums.ApplicationStatusEnum;
import cz.muni.pa165.facade.ApplicationFacade;
import cz.muni.pa165.generated.model.ApplicationStatus;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;

class ApplicationControllerTest {

    private static final Logger log = LoggerFactory.getLogger(ApplicationControllerTest.class);
    private final ApplicationFacade mockFacade = Mockito.mock(ApplicationFacade.class);
    private final ApplicationController carComponentController = new ApplicationController(mockFacade);

    @Test
    void testGetApplication() {
        log.debug("starting testGetApplication()");

        carComponentController.getApplication(1L);

        Mockito.verify(mockFacade, Mockito.times(1)).findById(1L);
    }

    @Test
    void testSetApplicationStatus() {
        log.debug("starting testGetCarComponentsByType()");

        carComponentController.setStatus(1L, ApplicationStatus.ACCEPTED);

        Mockito.verify(mockFacade, Mockito.times(1)).setStatus(1L, ApplicationStatusEnum.ACCEPTED);
    }
}