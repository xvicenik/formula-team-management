# Define the base image
FROM maven:3.9.0-eclipse-temurin-17-alpine AS build

# Copy the pom.xml parent file and module dirs
COPY pom.xml /build/pom.xml
COPY application /build/application
COPY core /build/core
COPY notification /build/notification
COPY visualization /build/visualization

WORKDIR /build
RUN mvn clean install -DskipTests

FROM eclipse-temurin:17-jre-focal AS pa165-formula-team-management-application

COPY --from=build /build/application/target/*.jar ./application.jar

# Expose ports and set entry point
EXPOSE 8081
CMD ["java", "-jar", "application.jar"]

FROM eclipse-temurin:17-jre-focal AS pa165-formula-team-management-core

COPY --from=build /build/core/target/*.jar ./core.jar

# Expose ports and set entry point
EXPOSE 8090
CMD ["java", "-jar", "core.jar"]

FROM eclipse-temurin:17-jre-focal AS pa165-formula-team-management-notification

COPY --from=build /build/notification/target/*.jar ./notification.jar

# Expose ports and set entry point
EXPOSE 8083
CMD ["java", "-jar", "notification.jar"]

FROM eclipse-temurin:17-jre-focal AS pa165-formula-team-management-visualization

COPY --from=build /build/visualization/target/*.jar ./visualization.jar

# Expose ports and set entry point
EXPOSE 8082
CMD ["java", "-jar", "visualization.jar"]