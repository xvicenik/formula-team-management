/*
package cz.muni.pa165.rest;

import cz.muni.pa165.facade.VisualizationFacade;
import cz.muni.pa165.generated.model.CarDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

*/
/**
 * @author Michal Badin
 *//*

class VisualizationControllerTest {
    private static final Logger log = LoggerFactory.getLogger(VisualizationControllerTest.class);
    private final VisualizationFacade mockFacade = Mockito.mock(VisualizationFacade.class);
    private final VisualizationController controller = new VisualizationController(mockFacade);


    @Test
    void testGenerateCarPdf() throws IOException {
        log.debug("testGenerateCarPdf() running");

        var car = new CarDto().id(1L);

        controller.generateCarPdf(car);

        Mockito.verify(mockFacade, Mockito.times(1)).generateCarPdf(car);
    }
}
*/
