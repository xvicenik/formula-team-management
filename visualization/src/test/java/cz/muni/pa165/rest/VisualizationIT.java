package cz.muni.pa165.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.pa165.generated.model.CarDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "mvn verify / mvn test / mvn clean install".
 * No need to test all the rest controllers logic here, just test that the endpoints are working*/


@SpringBootTest
@AutoConfigureMockMvc
class VisualizationIT {
    private static final Logger log = LoggerFactory.getLogger(VisualizationIT.class);
    private final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGenerateCarPdf() throws Exception {
        log.info("testGenerateCarPdf() running");

        var carDto = new CarDto().id(1L);
        var requestBody = mapper.writeValueAsString(Map.of("car", carDto));

        String response = mockMvc.perform(
                        post("/visualization")
                                .accept(MediaType.APPLICATION_PDF)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentType();
        log.info("response: {}", response);

        Assertions.assertNotNull(response);
        Assertions.assertEquals("application/pdf", response);
    }
}
