package cz.muni.pa165.facade;

import cz.muni.pa165.generated.model.CarDto;
import cz.muni.pa165.service.VisualizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class VisualizationFacade {

    private final VisualizationService visualizationService;

    @Autowired
    public VisualizationFacade(VisualizationService visualizationService) {

        this.visualizationService = visualizationService;
    }

    public Resource generateCarPdf(CarDto carDto) throws IOException {
        return visualizationService.generateCarPdf(carDto);
    }
}
