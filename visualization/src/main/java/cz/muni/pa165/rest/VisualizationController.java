package cz.muni.pa165.rest;

import cz.muni.pa165.facade.VisualizationFacade;
import cz.muni.pa165.generated.api.VisualizationApiDelegate;
import cz.muni.pa165.generated.model.CarDto;
import cz.muni.pa165.generated.model.GenerateCarPdfRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class VisualizationController implements VisualizationApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(VisualizationController.class);
    private final VisualizationFacade visualizationFacade;

    @Autowired
    public VisualizationController(VisualizationFacade visualizationFacade) {

        this.visualizationFacade = visualizationFacade;
    }

    @Override
    public ResponseEntity<Resource> generateCarPdf(GenerateCarPdfRequest generateCarPdfRequest) {
        log.debug("generateCarPdf() called");
        try {
            var resource = visualizationFacade.generateCarPdf(generateCarPdfRequest.getCar());
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            String.format("attachment; filename=car-%s.pdf", generateCarPdfRequest.getCar().getId()))
                    .contentType(MediaType.APPLICATION_PDF)
                    .contentLength(resource.contentLength())
                    .body(resource);
        } catch (IOException e) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
