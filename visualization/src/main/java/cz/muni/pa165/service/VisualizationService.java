package cz.muni.pa165.service;

import cz.muni.pa165.generated.model.CarDto;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class VisualizationService {
    public Resource generateCarPdf(CarDto carDto) throws IOException {
        try (PDDocument document = new PDDocument()) {
            // Add a new page to the document
            PDPage page = new PDPage();
            document.addPage(page);

            // Create a content stream for the page
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 16);

            contentStream.beginText();
            contentStream.newLineAtOffset(100, 700);
            contentStream.showText("Generated car PDF");

            contentStream.setFont(PDType1Font.HELVETICA, 14);
            contentStream.newLineAtOffset(0, -20);
            contentStream.showText("Car ID: " + carDto.getId());

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 14);
            contentStream.newLineAtOffset(0, -30);
            contentStream.showText("Components:");
            contentStream.newLineAtOffset(15, 0);

            contentStream.setFont(PDType1Font.HELVETICA, 12);
            if (carDto.getComponents() == null || carDto.getComponents().isEmpty()) {
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("No components");
            } else {
                for (var c: carDto.getComponents()) {
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("ID: " + c.getId());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Information: " + c.getInformation());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Type: " + c.getComponentType());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Weight: " + c.getWeight());
                    contentStream.newLineAtOffset(0, -20);
                }
            }

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 14);
            contentStream.newLineAtOffset(-15, -30);
            contentStream.showText("Driver:");
            contentStream.newLineAtOffset(15, 0);

            contentStream.setFont(PDType1Font.HELVETICA, 12);
            if (carDto.getDriver() == null) {
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("No driver");
            } else {
                var d = carDto.getDriver();
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("ID: " + d.getId());
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Name: " + d.getName());
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Surname: " + d.getSurname());
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Nationality: " + d.getNationality());
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Birthday: " + d.getBirthday());
                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Height: " + d.getHeight());

                contentStream.newLineAtOffset(0, -20);
                contentStream.showText("Characteristics:");
                contentStream.newLineAtOffset(15, 0);
                for (var c: carDto.getDriver().getCharacteristics()) {
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("- " + c.getValue());
                }
            }

            contentStream.endText();

            // Close the content stream
            contentStream.close();

            // Write the PDF document to a byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            document.save(baos);

            String outputDirName = "output-data";
            Path projectDir = Paths.get("");
            Path outputDirPath = projectDir.resolve(outputDirName);

            // Create output directory if it does not exist
            if (!Files.exists(outputDirPath)) {
                Files.createDirectories(outputDirPath);
            }

            // Save generated file
            Files.write(outputDirPath.resolve(getFilename()), baos.toByteArray());

            return new ByteArrayResource(baos.toByteArray());
        }
    }

    private String getFilename() {
        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");

        return String.format("car-updated-%s.pdf", currentTime.format(formatter));
    }
}
