from locust import HttpUser, task
import random

class Admin(HttpUser):
    fixed_count = 1

    @task
    def update_random_application_status(self):
        response = self.client.get("/application")

        if len(response.json()) == 0:
            return

        applicationNumber = random.choice([i for i in range(1, len(response.json()))])
        status = random.choice(["accepted", "rejected"])
        self.client.put(f"/application/status?id={applicationNumber}&applicationStatus={status}")

    @task
    def get_application(self):
        self.client.get("/application?id=1")

    @task
    def get_all_application(self):
        self.client.get("/application")


class User(HttpUser):
    @task
    def create_application(self):
        self.client.post("/application", json={
            "name": "Jozko",
            "surname": "Example",
            "birthday": "2000-05-06",
            "email": "john@example.com"
        })
