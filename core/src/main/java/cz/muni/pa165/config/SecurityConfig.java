package cz.muni.pa165.config;

import io.swagger.v3.oas.models.security.*;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/v3/api-docs/**", "/seed", "/clear").permitAll()
                        .requestMatchers(HttpMethod.POST,"/carComponent").hasAuthority("SCOPE_test_1")
                        .requestMatchers(HttpMethod.GET, "/carComponent").hasAnyAuthority("SCOPE_test_5", "SCOPE_test_1")
                        .requestMatchers("/carComponent/**").hasAnyAuthority("SCOPE_test_5", "SCOPE_test_1")
                        .requestMatchers("/car", "/car/**", "/driver/**", "/driver").hasAuthority("SCOPE_test_5")
                        .requestMatchers("/engineer", "/engineer/**", "/department", "/department/**").hasAuthority("SCOPE_test_5")
                        .anyRequest().denyAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }

    //settings for swaggerUI, all of this should be in openapi.yaml, but it doesn't work when put there
    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes("OAuth2",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.OAUTH2)
                                    .description("get access token with Authorization Code Grant")
                                    .flows(new OAuthFlows()
                                            .authorizationCode(new OAuthFlow()
                                                    .authorizationUrl("https://oidc.muni.cz/oidc/authorize")
                                                    .tokenUrl("https://oidc.muni.cz/oidc/token")
                                                    .scopes(new Scopes()
                                                            .addString("test_5", "manager scope")
                                                            .addString("test_1", "engineer scope")
                                                    )
                                            )
                                    )
                    );

            var managerScopeRequirement = new SecurityRequirement().addList("OAuth2", "test_5");
            var engineerScopeRequirement = new SecurityRequirement().addList("OAuth2", "test_1");

            openApi.getPaths().get("/car").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car").getPost().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car/driver").getPut().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car/driver").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car/{id}").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car/{id}").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/car/{id}").getPatch().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver").getPost().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver/{id}").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver/{id}").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver/{id}").getPatch().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/driver/test").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent/{id}").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent/{id}").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent/{id}").getPatch().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent/type").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department/{departmentId}").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department/{departmentId}").getPut().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department/{departmentId}").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department/{departmentId}").getPatch().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/department").getPost().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/engineer").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/engineer").getPost().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/engineer/{id}").getGet().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/engineer/{id}").getDelete().addSecurityItem(managerScopeRequirement);
            openApi.getPaths().get("/carComponent").getPost().addSecurityItem(engineerScopeRequirement);
        };
    }


}