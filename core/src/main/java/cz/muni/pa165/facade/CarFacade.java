package cz.muni.pa165.facade;

import cz.muni.pa165.generated.core.model.CarDto;
import cz.muni.pa165.generated.core.model.CarDtoPage;
import cz.muni.pa165.mappers.CarMapper;
import cz.muni.pa165.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarFacade {

    private final CarService carService;
    private final CarMapper carMapper;

    @Autowired
    public CarFacade(CarService carService, CarMapper carMapper) {
        this.carService = carService;
        this.carMapper = carMapper;
    }

    @Cacheable(cacheNames = "cars", key = "#id")
    @Transactional(readOnly = true)
    public CarDto get(Long id) {
        return carMapper.mapToDto(carService.get(id));
    }

    public CarDto create(List<Long> componentIds) {
        return carMapper.mapToDto(carService.create(componentIds));
    }

    public void delete(Long id) {
        carService.delete(id);
    }

    public CarDto update(Long id, List<Long> componentIds) {
        return carMapper.mapToDto(carService.update(id, componentIds));
    }

    public CarDto setDriver(Long carId, Long driverId) {
        return carMapper.mapToDto(carService.setDriver(carId, driverId));
    }

    public void removeDriver(Long carId) {
        carService.removeDriver(carId);
    }

    public CarDtoPage getAllCars(Integer page, Integer size) {
        return carMapper.mapToDtoPage(carService.findAll(page, size));
    }
}
