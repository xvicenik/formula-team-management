package cz.muni.pa165.facade;

import cz.muni.pa165.generated.core.model.DriverCreateDto;
import cz.muni.pa165.generated.core.model.DriverDto;
import cz.muni.pa165.generated.core.model.DriverUpdateDto;
import cz.muni.pa165.mappers.DriverMapper;
import cz.muni.pa165.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Michal Badin
 */
@Service
@Transactional
public class DriverFacade {
    private final DriverService driverService;
    private final DriverMapper driverMapper;

    @Autowired
    DriverFacade(DriverService driverService, DriverMapper driverMapper) {
        this.driverService = driverService;
        this.driverMapper = driverMapper;
    }

    @Cacheable(cacheNames = "drivers", key = "#id")
    @Transactional(readOnly = true)
    public DriverDto findById(Long id) {
        return driverMapper.mapToDto(driverService.findById(id));
    }

    public DriverDto create(DriverCreateDto driverCreateDto) {
        return driverMapper.mapToDto(driverService.create(driverMapper.mapFromCreateDto(driverCreateDto)));
    }

    public void delete(Long id) {
        driverService.delete(id);
    }

    public DriverDto update(Long id, DriverUpdateDto driverUpdateDto) {
        return driverMapper.mapToDto(driverService.update(id, driverMapper.mapFromUpdateDto(driverUpdateDto)));
    }

    @Transactional(readOnly = true)
    public List<DriverDto> getAllDrivers() {
        return driverMapper.mapToList(driverService.findAll());
    }

    @Transactional(readOnly = true)
    public List<DriverDto> getAllTestDrivers() {
        return driverMapper.mapToList(driverService.findAllTestDrivers());
    }
}
