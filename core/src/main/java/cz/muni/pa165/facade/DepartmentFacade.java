package cz.muni.pa165.facade;

import cz.muni.pa165.generated.core.model.DepartmentCreateDto;
import cz.muni.pa165.generated.core.model.DepartmentDto;
import cz.muni.pa165.generated.core.model.DepartmentUpdateDto;
import cz.muni.pa165.mappers.DepartmentMapper;
import cz.muni.pa165.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DepartmentFacade {

    private final DepartmentService departmentService;
    private final DepartmentMapper mapper;

    @Autowired
    public DepartmentFacade(DepartmentService departmentService, DepartmentMapper departmentMapper) {
        this.departmentService = departmentService;
        this.mapper = departmentMapper;
    }

    public DepartmentDto createDepartment(DepartmentCreateDto departmentCreateDto) {
        return mapper.mapToDto(departmentService.create(mapper.mapFromCreateDto(departmentCreateDto)));
    }

    public DepartmentDto addEngineer(Long departmentId, Long engineerId) {
        return mapper.mapToDto(departmentService.addEngineer(
                departmentId, engineerId)
        );
    }

    public DepartmentDto updateSpecialization(Long id, DepartmentUpdateDto departmentUpdateDto) {
        return mapper.mapToDto(departmentService.updateSpecialization(id, mapper.mapFromUpdateDto(departmentUpdateDto)));
    }

    public void deleteDepartment(Long id) {
        departmentService.delete(id);
    }

    @Transactional(readOnly = true)
    public DepartmentDto findById(Long id) {
        return mapper.mapToDto(departmentService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<DepartmentDto> findAll() {
        return mapper.mapToList(departmentService.findAll());
    }
}
