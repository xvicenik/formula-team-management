package cz.muni.pa165.facade;

import cz.muni.pa165.generated.core.model.EngineerCreateDto;
import cz.muni.pa165.generated.core.model.EngineerDto;
import cz.muni.pa165.mappers.EngineerMapper;
import cz.muni.pa165.service.EngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EngineerFacade {
    private final EngineerService engineerService;
    private final EngineerMapper engineerMapper;

    @Autowired
    public EngineerFacade(EngineerService engineerService, EngineerMapper engineerMapper) {
        this.engineerService = engineerService;
        this.engineerMapper = engineerMapper;
    }

    public EngineerDto createEngineer(EngineerCreateDto engineerCreateDto) {
        return engineerMapper.mapToDto(engineerService.create(engineerMapper.mapFromCreateDto(engineerCreateDto)));
    }

    public void deleteEngineer(Long id) {
        engineerService.delete(id);
    }

    public EngineerDto getEngineer(Long id) {
        return engineerMapper.mapToDto(engineerService.findById(id));
    }

    public List<EngineerDto> getEngineers() {
        return engineerMapper.mapToList(engineerService.findAll());
    }
}
