package cz.muni.pa165.facade;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.generated.core.model.CarComponentCreateDto;
import cz.muni.pa165.generated.core.model.CarComponentDto;
import cz.muni.pa165.generated.core.model.CarComponentUpdateDto;
import cz.muni.pa165.mappers.CarComponentMapper;
import cz.muni.pa165.service.CarComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarComponentFacade {

    private final CarComponentService componentService;
    private final CarComponentMapper componentMapper;

    @Autowired
    public CarComponentFacade(CarComponentService componentService, CarComponentMapper componentMapper) {
        this.componentService = componentService;
        this.componentMapper = componentMapper;
    }

    @Cacheable(cacheNames = "components", key = "#id")
    @Transactional(readOnly = true)
    public CarComponentDto findById(Long id) {
        return componentMapper.mapToDto(componentService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<CarComponentDto> findAll() {
        return componentMapper.mapToList(componentService.findAll());
    }

    @Transactional(readOnly = true)
    public List<CarComponentDto> findAllByType(ComponentTypeEnum type) {
        return componentMapper.mapToList(componentService.findAllByType(type));
    }

    public CarComponentDto create(CarComponentCreateDto componentCreateDto) {
        return componentMapper.mapToDto(componentService.create(componentMapper.mapFromCreateDto(componentCreateDto)));
    }

    public void delete(Long id) {
        componentService.delete(id);
    }

    public CarComponentDto update(Long id, CarComponentUpdateDto componentUpdateDto) {
        return componentMapper.mapToDto(componentService.update(id, componentMapper.mapFromUpdateDto(componentUpdateDto)));
    }
}
