package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Department;
import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.data.repository.DepartmentRepository;
import cz.muni.pa165.data.repository.EngineerRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService extends DomainService<Department> {

    private final DepartmentRepository departmentRepository;
    private final EngineerRepository engineerRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository, EngineerRepository engineerRepository) {
        super(departmentRepository, Department.class);
        this.departmentRepository = departmentRepository;
        this.engineerRepository = engineerRepository;
    }

    @Transactional(readOnly = true)
    public Department findById(Long id) {
        return departmentRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(entityClass, id));
    }

    public Department addEngineer(Long departmentId, Long engineerId) {
        Department department = findById(departmentId);

        Optional<Engineer> engineer = engineerRepository.findById(engineerId);
        if (engineer.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, engineerId);
        }

        if (department.getEngineers().contains(engineer.get())) {
            throw new BadRequestException("Engineer already added to department");
        }

        department.addEngineer(engineer.get());
        engineer.get().setDepartment(department);

        return departmentRepository.save(department);
    }

    public Department updateSpecialization(Long id, Department department) {
        Department departmentFromDb = findById(id);
        departmentFromDb.setSpecialization(department.getSpecialization());

        return departmentRepository.save(departmentFromDb);
    }

    @Transactional(readOnly = true)
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

}
