package cz.muni.pa165.service;

import cz.muni.pa165.data.model.DomainObject;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Michal Badin
 */
public abstract class DomainService<ENTITY extends DomainObject> {
    protected final JpaRepository<ENTITY, Long> repository;
    protected final Class<ENTITY> entityClass;

    protected DomainService(JpaRepository<ENTITY, Long> repository, Class<ENTITY> entityClass) {
        this.repository = repository;
        this.entityClass = entityClass;
    }

    public ENTITY create(ENTITY entity) {
        return repository.save(entity);
    }

    public void delete(Long id) {
        Optional<ENTITY> fromDb = repository.findById(id);
        if (fromDb.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }
        repository.delete(fromDb.get());
    }

    @Transactional(readOnly = true)
    public List<ENTITY> findAll() {
        return repository.findAll();
    }

}
