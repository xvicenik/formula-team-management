package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.data.repository.EngineerRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class EngineerService extends DomainService<Engineer> {
    private final EngineerRepository engineerRepository;

    protected EngineerService(EngineerRepository repository) {
        super(repository, Engineer.class);
        this.engineerRepository = repository;
    }

    @Transactional(readOnly = true)
    public Engineer findById(Long id) {
        return engineerRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(entityClass, id));
    }

}
