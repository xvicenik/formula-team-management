package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.model.Driver;
import cz.muni.pa165.data.repository.CarComponentRepository;
import cz.muni.pa165.data.repository.CarRepository;
import cz.muni.pa165.data.repository.DriverRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestClientException;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CarService extends DomainService<Car> {
    private static final Logger log = LoggerFactory.getLogger(CarService.class);
    private static final String VISUALIZATION_MODULE_DOCKER = "http://visualization:8082";
    private static final String VISUALIZATION_MODULE_LOCALHOST = "http://localhost:8082";
    private String VISUALIZATION_MODULE_URL;
    private static final String VISUALIZATION_MODULE_URI = "/visualization";
    private final DriverRepository driverRepository;
    private final CarRepository carRepository;
    private final CarComponentRepository carComponentRepository;
    private final WebClient webClient;

    @Autowired
    public CarService(CarRepository carRepository, DriverRepository driverRepository,
                      CarComponentRepository carComponentRepository, WebClient.Builder webClientBuilder) {
        super(carRepository, Car.class);
        this.driverRepository = driverRepository;
        this.carRepository = carRepository;
        this.carComponentRepository = carComponentRepository;

        //if running in docker, modules cannot communicate through localhost
        if (System.getenv("DOCKER") != null) {
            VISUALIZATION_MODULE_URL = VISUALIZATION_MODULE_DOCKER;
        }
        else {
            VISUALIZATION_MODULE_URL = VISUALIZATION_MODULE_LOCALHOST;
        }
        this.webClient = webClientBuilder.baseUrl(VISUALIZATION_MODULE_URL).build();
    }

    public Car create(List<Long> componentIds) {
        var car = new Car();
        createComponentsForCar(car, componentIds);
        return car;
    }

    @Override
    public void delete(Long id) {
        Optional<Car> carFromDb = carRepository.findById(id);
        if (carFromDb.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }

        Driver driver = carFromDb.get().getDriver();
        if (driver != null) {
            Optional<Driver> driverFromDb = driverRepository.findById(driver.getId());
            driverFromDb.get().setCar(null);
            driverRepository.save(driverFromDb.get());
        }

        repository.delete(carFromDb.get());
    }

    public Car update(Long id, List<Long> componentIds) {
        Optional<Car> dbCar = carRepository.findById(id);
        if (dbCar.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }

        createComponentsForCar(dbCar.get(), componentIds);

        return dbCar.get();
    }

    @Transactional(readOnly = true)
    public Car findById(Long id) {
        return carRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(entityClass, id));
    }

    @Transactional(readOnly = true)
    public Car get(Long id) {
        Car car = findById(id);

        try {
            sendPostRequest(car);
        } catch (WebClientException e) {
            log.debug(String.format("The visualization module is not reachable on the URL: %s, exception %s", VISUALIZATION_MODULE_URL + VISUALIZATION_MODULE_URI, e));
        }

        return car;
    }

    public Car setDriver(Long carId, Long driverId) {
        Optional<Driver> driverFromDb = driverRepository.findById(driverId);
        if (driverFromDb.isEmpty()) {
            throw new ResourceNotFoundException(Driver.class, driverId);
        }

        if (driverFromDb.get().getCar() != null) {
            throw new BadRequestException("Driver is already driver of a different car");
        }

        Optional<Car> carFromDb = carRepository.findById(carId);
        if (carFromDb.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, carId);
        }

        if (carFromDb.get().getDriver() != null) {
            throw new BadRequestException("Car already has a driver");
        }

        carFromDb.get().setDriver(driverFromDb.get());

        var savedCar = repository.save(carFromDb.get());

        try {
            sendPostRequest(savedCar);
        } catch (WebClientException e) {
            log.debug(String.format("The visualization module is not reachable on the URL: %s, exception %s", VISUALIZATION_MODULE_URL + VISUALIZATION_MODULE_URI, e));
        }

        return savedCar;
    }

    public Car removeDriver(Long carId) {
        Optional<Car> carFromDb = carRepository.findById(carId);
        if (carFromDb.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, carId);
        }

        Driver driver = carFromDb.get().getDriver();
        if (driver == null) {
            throw new BadRequestException(String.format("Car with id %s does not have a driver", carId));
        }

        Optional<Driver> driverFromDb = driverRepository.findById(driver.getId());
        if (driverFromDb.isEmpty()) {
            throw new ResourceNotFoundException(Driver.class, driver.getId());
        }

        carFromDb.get().setDriver(null);
        driverFromDb.get().setCar(null);

        driverRepository.save(driverFromDb.get());

        return repository.save(carFromDb.get());
    }

    public Page<Car> findAll(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);

        return repository.findAll(pageable);
    }

    private void createComponentsForCar(Car car, List<Long> componentIds) {
        if (componentIds == null) {
            repository.save(car);
            return;
        }

        var carComponents = car.getComponents();

        for (Long componentId : componentIds) {
            Optional<CarComponent> dbComponent = carComponentRepository.findById(componentId);

            if (dbComponent.isEmpty()) {
                throw new ResourceNotFoundException(CarComponent.class, componentId);
            }

            if (carComponents.contains(dbComponent.get())) {
                throw new BadRequestException("Car can only have one component of each type");
            }

            carComponents.add(dbComponent.get());
            dbComponent.get().setCar(car);

            repository.save(car);
        }
    }

    public void sendPostRequest(Car car) throws RestClientException {
        Map<String, Object> visualization = new HashMap<>();
        visualization.put("car", car);

        webClient.post()
                .uri(VISUALIZATION_MODULE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(visualization)
                .retrieve()
                .bodyToMono(Resource.class)
                .block();
    }
}
