package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.*;
import cz.muni.pa165.data.repository.*;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Set;

import static cz.muni.pa165.data.enums.CharacteristicsEnum.*;

/**
 * @author Michal Badin
 */
@Service
@Profile("dev")
public class DBManagementService {
    private final CarComponentRepository carComponentRepository;
    private final CarRepository carRepository;
    private final DepartmentRepository departmentRepository;
    private final DriverRepository driverRepository;
    private final EngineerRepository engineerRepository;

    @Autowired
    public DBManagementService(CarComponentRepository carComponentRepository,
                               CarRepository carRepository,
                               DepartmentRepository departmentRepository,
                               DriverRepository driverRepository,
                               EngineerRepository engineerRepository) {
        this.carComponentRepository = carComponentRepository;
        this.carRepository = carRepository;
        this.departmentRepository = departmentRepository;
        this.driverRepository = driverRepository;
        this.engineerRepository = engineerRepository;
    }


    @Transactional
    @PostConstruct
    public void seed() {
        //region First car
        Car car = new Car();

        Driver driver = new Driver();
        driver.setName("Xiao");
        driver.setSurname("Chen");
        driver.setBirthday(LocalDate.of(1995, 7, 29));
        driver.setHeight(189);
        driver.setNationality("Slovak");
        driver.setCharacteristics(Set.of(AGGRESSIVENESS, EXCELLENT_STARTER, EXCELLENT_IN_THE_CORNERS, REACT_QUICKLY));
        driverRepository.save(driver);

        car.setDriver(driver);
        carRepository.save(car);

        saveCarComponent(ComponentTypeEnum.FRONTWING, 50.5, "Lightweight front wing v2 (black)", car);
        saveCarComponent(ComponentTypeEnum.CHASSIS, 365.9, "Lightweight chassis", car);
        saveCarComponent(ComponentTypeEnum.ENGINE, 300.5, "Lightweight V10 engine", car);
        saveCarComponent(ComponentTypeEnum.SUSPENSION, 5.0, "Lightweight suspension v3 (black)", car);
        //endregion

        //region Second car
        car = new Car();

        driver = new Driver();
        driver.setName("Marek");
        driver.setSurname("Pilkovic");
        driver.setBirthday(LocalDate.of(1987, 1, 1));
        driver.setHeight(189);
        driver.setNationality("British");
        driver.setCharacteristics(Set.of(AGGRESSIVENESS, DRIVING_ON_THE_WET, EXCELLENT_IN_THE_CORNERS, REACT_QUICKLY));
        driverRepository.save(driver);

        car.setDriver(driver);
        carRepository.save(car);

        saveCarComponent(ComponentTypeEnum.FRONTWING, 57.5, "Lightweight front wing v3 (black)", car);
        saveCarComponent(ComponentTypeEnum.CHASSIS, 325.9, "Lightweight chassis v5", car);
        saveCarComponent(ComponentTypeEnum.ENGINE, 500.5, "V18 engine", car);
        saveCarComponent(ComponentTypeEnum.SUSPENSION, 6.5, "Lightweight suspension v5 (white)", car);
        //endregion

        //region Test driver
        Driver testDriver = new Driver();
        testDriver.setName("Anuja");
        testDriver.setSurname("Ankska");
        testDriver.setNationality("Czech");
        testDriver.setBirthday(LocalDate.of(2000, 1, 30));
        testDriver.setHeight(175);
        testDriver.setCharacteristics(Set.of(DRIVING_ON_THE_WET, REMAIN_COMPLETELY_FOCUSED, EFFICIENT_CARDIOVASCULAR_SYSTEMS));
        driverRepository.save(testDriver);
        //endregion

        //region Car components
        saveCarComponent(ComponentTypeEnum.FRONTWING, 70.5, "Front wing v7 (red)", null);
        saveCarComponent(ComponentTypeEnum.CHASSIS, 500.9, "Medium weight chassis v2", null);
        saveCarComponent(ComponentTypeEnum.ENGINE, 581.5, "V12 engine", null);
        saveCarComponent(ComponentTypeEnum.SUSPENSION, 9.5, "Lightweight suspension v5 (white)", null);
        //endregion

        //region Department and engineers
        Department department = new Department();
        department.setSpecialization("Engine department");
        departmentRepository.save(department);

        Engineer engineer1 = new Engineer();
        engineer1.setName("Michal");
        engineer1.setSurname("Pekarik");
        engineer1.setDepartment(department);
        engineerRepository.save(engineer1);

        Engineer engineer2 = new Engineer();
        engineer2.setName("Andrej");
        engineer2.setSurname("Uzasny");
        engineer2.setDepartment(department);
        engineerRepository.save(engineer2);

        Engineer engineer3 = new Engineer();
        engineer3.setName("Jitka");
        engineer3.setSurname("Dlouha");
        engineer3.setDepartment(department);
        engineerRepository.save(engineer3);

        Engineer engineer4 = new Engineer();
        engineer4.setName("Betka");
        engineer4.setSurname("Muslova");
        engineer4.setDepartment(department);
        engineerRepository.save(engineer4);
        //endregion
    }

    private void saveCarComponent(ComponentTypeEnum type, double weight, String information, Car car) {
        CarComponent carComponentFrontWing = new CarComponent(type, weight, information, car);
        carComponentRepository.save(carComponentFrontWing);
    }

    @Transactional
    public void clear() {
        carComponentRepository.deleteAll();
        carRepository.deleteAll();
        driverRepository.deleteAll();
        departmentRepository.deleteAll();
        engineerRepository.deleteAll();
    }
}
