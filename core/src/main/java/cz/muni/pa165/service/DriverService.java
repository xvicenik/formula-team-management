package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Driver;
import cz.muni.pa165.data.repository.DriverRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Michal Badin
 */
@Service
public class DriverService extends DomainService<Driver> {

    private final DriverRepository driverRepository;

    @Autowired
    public DriverService(DriverRepository repository) {
        super(repository, Driver.class);
        driverRepository = repository;
    }

    @Transactional(readOnly = true)
    public Driver findById(Long id) {
        Optional<Driver> dbDriver = driverRepository.findById(id);
        if (dbDriver.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }

        return dbDriver.get();
    }

    public Driver update(Long id, Driver driver) {
        Optional<Driver> dbDriver = driverRepository.findById(id);
        if (dbDriver.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }

        dbDriver.get().setName(driver.getName());
        dbDriver.get().setSurname(driver.getSurname());
        dbDriver.get().setNationality(driver.getNationality());
        dbDriver.get().setHeight(driver.getHeight());
        dbDriver.get().setCharacteristics(driver.getCharacteristics());
        return driverRepository.save(dbDriver.get());
    }

    @Transactional(readOnly = true)
    public List<Driver> findAllTestDrivers() {
        return driverRepository.findAllTestDrivers();
    }

    @Override
    public void delete(Long id) {
        var driverFromDb = repository.findById(id);
        if (driverFromDb.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }

        if (driverFromDb.get().getCar() != null) {
            throw new BadRequestException(
                    String.format("Driver with id %s is driver of the car with id %s. Remove the driver from the car first.",
                            id, driverFromDb.get().getCar().getId()));
        }

        repository.delete(driverFromDb.get());
    }
}

