package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.repository.CarComponentRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.*;

@Service
public class CarComponentService extends DomainService<CarComponent> {

    private static final Logger log = LoggerFactory.getLogger(CarComponentService.class);
    private static final String NOTIFICATION_MODULE_DOCKER = "http://notification:8083";
    private static final String NOTIFICATION_MODULE_LOCALHOST = "http://localhost:8083";
    private static final String NOTIFICATION_MODULE_URI = "/notification/component";
    private final CarComponentRepository componentRepository;
    private final WebClient webClient;
    @Value("${notification.receivers}")
    private final List<String> notificationReceivers = new ArrayList<>();
    private String NOTIFICATION_MODULE_URL;

    @Autowired
    public CarComponentService(CarComponentRepository repository, WebClient.Builder webClientBuilder) {
        super(repository, CarComponent.class);
        this.componentRepository = repository;

        //if running in docker, modules cannot communicate through localhost
        if (System.getenv("DOCKER") != null) {
            NOTIFICATION_MODULE_URL = NOTIFICATION_MODULE_DOCKER;
        } else {
            NOTIFICATION_MODULE_URL = NOTIFICATION_MODULE_LOCALHOST;
        }
        this.webClient = webClientBuilder.baseUrl(NOTIFICATION_MODULE_URL).build();
    }

    @Override
    public CarComponent create(CarComponent component) {
        CarComponent savedComponent = repository.save(component);
        try {
            sendPostRequest(savedComponent);
        } catch (WebClientException e) {
            log.debug(String.format("The notification module is not reachable on the URL: %s, exception %s", NOTIFICATION_MODULE_URL + NOTIFICATION_MODULE_URI, e));
        }
        return savedComponent;
    }

    public CarComponent update(Long id, CarComponent component) {
        Optional<CarComponent> dbComponent = componentRepository.findById(id);
        if (dbComponent.isEmpty()) {
            throw new ResourceNotFoundException(entityClass, id);
        }
        dbComponent.get().setComponentType(component.getComponentType());
        dbComponent.get().setInformation(component.getInformation());
        dbComponent.get().setWeight(component.getWeight());

        return componentRepository.save(dbComponent.get());
    }

    @Transactional(readOnly = true)
    public CarComponent findById(Long id) {
        return componentRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(entityClass, id));
    }

    @Transactional(readOnly = true)
    public List<CarComponent> findAllByType(ComponentTypeEnum type) {
        return componentRepository.findByType(type);
    }

    private void sendPostRequest(CarComponent component) {
        Map<String, Object> notification = new HashMap<>();
        notification.put("carComponent", component);
        notification.put("receivers", notificationReceivers);

        webClient.post()
                .uri(NOTIFICATION_MODULE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(notification)
                .retrieve()
                .bodyToMono(Resource.class)
                .block();
    }
}
