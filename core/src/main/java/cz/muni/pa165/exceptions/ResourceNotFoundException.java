package cz.muni.pa165.exceptions;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(Class<?> clazz, Long id) {
        super(String.format("%s with id %s was not found", clazz, id));
    }
}

