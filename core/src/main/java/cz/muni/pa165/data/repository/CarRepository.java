package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    @Query("SELECT c FROM Car c LEFT JOIN FETCH c.components cc LEFT JOIN FETCH c.driver d WHERE c.id = :id")
    Optional<Car> findById(@Param("id") Long id);
}
