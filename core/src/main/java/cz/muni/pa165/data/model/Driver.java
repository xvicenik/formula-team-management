package cz.muni.pa165.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.muni.pa165.data.enums.CharacteristicsEnum;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "driver")
public class Driver extends DomainObject<Long> implements Serializable {

    @NotEmpty
    @Size(max = 35)
    private String name;

    @NotEmpty
    @Size(max = 35)
    private String surname;

    @Nullable
    @Min(100)
    @Max(300)
    private Integer height;

    @Nullable
    @Past
    private LocalDate birthday;

    @NotEmpty
    @Size(max = 20)
    private String nationality;

    @ElementCollection(targetClass = CharacteristicsEnum.class)
    @CollectionTable(name = "driver_characteristics", joinColumns = @JoinColumn(name = "driver_id", nullable = false))
    @Column(name = "characteristic")
    @Enumerated(EnumType.STRING)
    @Nonnull
    private Set<CharacteristicsEnum> characteristics;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "driver",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    @Nullable
    @JsonIgnore
    private Car car;

    public Driver() {
    }

    public Driver(String name, String surname, Integer height, LocalDate birthday, String nationality,
                  Car car, Set<CharacteristicsEnum> characteristics) {
        this.name = name;
        this.surname = surname;
        this.height = height;
        this.birthday = birthday;
        this.nationality = nationality;
        this.car = car;
        this.characteristics = characteristics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Set<CharacteristicsEnum> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Set<CharacteristicsEnum> characteristics) {
        this.characteristics = characteristics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Driver driver)) {
            return false;
        }
        return Objects.equals(getName(), driver.getName()) && Objects.equals(getSurname(), driver.getSurname()) && Objects.equals(getHeight(), driver.getHeight()) && Objects.equals(getBirthday(), driver.getBirthday()) && Objects.equals(getNationality(), driver.getNationality());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getHeight(), getBirthday(), getNationality());
    }
}
