package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.CarComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarComponentRepository extends JpaRepository<CarComponent, Long> {
    @Query("SELECT c FROM CarComponent c WHERE c.id = :id")
    Optional<CarComponent> findById(@Param("id") Long id);

    @Query("SELECT c FROM CarComponent c WHERE c.componentType = ?1")
    List<CarComponent> findByType(ComponentTypeEnum type);
}
