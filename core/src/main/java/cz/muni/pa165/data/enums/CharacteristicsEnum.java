package cz.muni.pa165.data.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CharacteristicsEnum {
    AGGRESSIVENESS("aggressiveness"),

    DRIVING_ON_THE_WET("driving on the wet"),

    REACT_QUICKLY("react quickly"),

    REMAIN_COMPLETELY_FOCUSED("remain completely focused"),

    ADAPTABILITY_TO_THE_ENVIRONMENT("adaptability to the environment"),

    STRONG_NECK("strong neck"),

    ABILITY_TO_INTERACT_WITH_THE_TEAM("ability to interact with the team"),

    EFFICIENT_CARDIOVASCULAR_SYSTEMS("efficient cardiovascular systems"),

    EXCELLENT_WHEN_OVERTAKING("excellent when overtaking"),

    EXCELLENT_STARTER("excellent starter"),

    EXCELLENT_IN_THE_CORNERS("excellent in the corners"),

    EXCELLENT_IN_STRAIGHT_LINES("excellent in straight lines");

    private final String value;

    CharacteristicsEnum(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static CharacteristicsEnum fromValue(String value) {
        for (CharacteristicsEnum b : CharacteristicsEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
