package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    @Query("SELECT d FROM Department d LEFT JOIN FETCH d.engineers e WHERE d.id = :id")
    Optional<Department> findById(@Param("id") Long id);

    @Query("SELECT d FROM Department d LEFT JOIN FETCH d.engineers e")
    List<Department> findAll();
}
