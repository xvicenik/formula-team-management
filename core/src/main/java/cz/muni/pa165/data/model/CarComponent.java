package cz.muni.pa165.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.muni.pa165.data.enums.ComponentTypeEnum;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "car_component")
public class CarComponent extends DomainObject<Long> implements Serializable {

    @Column(name = "component_type")
    @Enumerated(EnumType.STRING)
    @Nonnull
    private ComponentTypeEnum componentType;

    @Nonnull
    @Positive
    private Double weight;

    @NotEmpty
    @Size(max = 100)
    private String information;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    @Nullable
    @JsonIgnore
    private Car car;

    public CarComponent() {

    }

    public CarComponent(ComponentTypeEnum type, Double weight, String information, Car car) {
        this.componentType = type;
        this.weight = weight;
        this.information = information;
        this.car = car;
    }

    public ComponentTypeEnum getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentTypeEnum componentType) {
        this.componentType = componentType;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String description) {
        this.information = description;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CarComponent component)) {
            return false;
        }
        return Objects.equals(getId(), component.getId()) && Objects.equals(getComponentType(), component.getComponentType()) && Objects.equals(getWeight(), component.getWeight()) && Objects.equals(getInformation(), component.getInformation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getComponentType(), getWeight(), getInformation());
    }
}
