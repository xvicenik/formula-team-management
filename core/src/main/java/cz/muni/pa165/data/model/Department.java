package cz.muni.pa165.data.model;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "department")
public class Department extends DomainObject<Long> implements Serializable {

    @NotEmpty
    @Size(max = 100)
    private String specialization;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "department",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    @Nullable
    private Set<Engineer> engineers;

    public Department() {

    }

    public Department(String specialization, Set<Engineer> engineers) {
        this.specialization = specialization;
        this.engineers = engineers;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Set<Engineer> getEngineers() {
        return engineers;
    }

    public void setEngineers(Set<Engineer> engineers) {
        this.engineers = engineers;
    }

    public void addEngineer(Engineer engineer) {
        this.engineers.add(engineer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Department department)) {
            return false;
        }
        return Objects.equals(getSpecialization(), department.getSpecialization());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecialization());
    }
}