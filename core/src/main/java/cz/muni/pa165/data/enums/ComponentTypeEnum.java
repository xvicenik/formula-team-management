package cz.muni.pa165.data.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ComponentTypeEnum {
    CHASSIS("CHASSIS"),
    ENGINE("ENGINE"),
    FRONTWING("FRONTWING"),
    SUSPENSION("SUSPENSION");

    private final String value;

    ComponentTypeEnum(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ComponentTypeEnum fromValue(String value) {
        for (ComponentTypeEnum b : ComponentTypeEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}

