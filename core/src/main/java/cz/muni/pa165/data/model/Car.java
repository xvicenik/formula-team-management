package cz.muni.pa165.data.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "car")
public class Car extends DomainObject<Long> implements Serializable {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id")
    @Nullable
    private Driver driver;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "car",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    @Nonnull
    private Set<CarComponent> components;

    public Car() {
        this.components = new HashSet<>();
    }

    public Car(Driver driver, Set<CarComponent> components) {
        this.driver = driver;
        this.components = components;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Set<CarComponent> getComponents() {
        return components;
    }

    public void setComponents(Set<CarComponent> components) {
        this.components = components;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Car car)) {
            return false;
        }
        return Objects.equals(getDriver(), car.getDriver()) && Objects.equals(getComponents(), car.getComponents());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDriver(), getComponents());
    }
}
