package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * @author Michal Badin
 */
public interface DriverRepository extends JpaRepository<Driver, Long> {
    @Query("SELECT d FROM Driver d WHERE d.id = :id")
    Optional<Driver> findById(@Param("id") Long id);

    @Query("SELECT d FROM Driver d LEFT JOIN FETCH d.car WHERE d.car IS NULL")
    List<Driver> findAllTestDrivers();
}
