package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.Department;
import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.generated.core.model.DepartmentCreateDto;
import cz.muni.pa165.generated.core.model.DepartmentDto;
import cz.muni.pa165.generated.core.model.DepartmentUpdateDto;
import cz.muni.pa165.generated.core.model.EngineerDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {
    DepartmentDto mapToDto(Department department);

    Engineer mapFromEngineerDto(EngineerDto engineerDto);

    Department mapFromCreateDto(DepartmentCreateDto departmentCreateDto);

    Department mapFromUpdateDto(DepartmentUpdateDto departmentUpdateDto);

    List<DepartmentDto> mapToList(List<Department> departmentList);
}
