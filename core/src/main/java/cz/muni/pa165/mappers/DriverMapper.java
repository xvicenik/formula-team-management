package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.Driver;
import cz.muni.pa165.generated.core.model.DriverCreateDto;
import cz.muni.pa165.generated.core.model.DriverDto;
import cz.muni.pa165.generated.core.model.DriverUpdateDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Michal Badin
 */
@Mapper(componentModel = "spring")
public interface DriverMapper {
    DriverDto mapToDto(Driver driver);

    Driver mapFromCreateDto(DriverCreateDto carCreateDto);

    Driver mapFromUpdateDto(DriverUpdateDto carUpdateDto);

    List<DriverDto> mapToList(List<Driver> drivers);
}
