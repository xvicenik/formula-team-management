package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.generated.core.model.EngineerCreateDto;
import cz.muni.pa165.generated.core.model.EngineerDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EngineerMapper {
    EngineerDto mapToDto(Engineer engineer);

    Engineer mapFromCreateDto(EngineerCreateDto engineerCreateDto);

    List<EngineerDto> mapToList(List<Engineer> engineers);
}
