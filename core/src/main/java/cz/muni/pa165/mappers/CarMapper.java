package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.generated.core.model.CarDto;
import cz.muni.pa165.generated.core.model.CarDtoPage;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CarMapper {

    CarDto mapToDto(Car car);

    List<CarDto> mapToList(List<Car> cars);

    default CarDtoPage mapToDtoPage(Page<Car> cars) {
        return new CarDtoPage()
                .content(mapToList(cars.getContent()))
                .pageable(cars.getPageable())
                .size(cars.getSize())
                .totalPages(cars.getTotalPages())
                .totalElements((int) cars.getTotalElements())
                .sort(cars.getSort())
                .number(cars.getNumber())
                .isLast(cars.isLast());
    }
}
