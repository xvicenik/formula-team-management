package cz.muni.pa165.mappers;

import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.generated.core.model.CarComponentCreateDto;
import cz.muni.pa165.generated.core.model.CarComponentDto;
import cz.muni.pa165.generated.core.model.CarComponentUpdateDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CarComponentMapper {

    CarComponentDto mapToDto(CarComponent component);

    CarComponent mapFromDto(CarComponentDto componentDto);

    CarComponent mapFromCreateDto(CarComponentCreateDto componentCreateDto);

    CarComponent mapFromUpdateDto(CarComponentUpdateDto componentUpdateDto);

    List<CarComponentDto> mapToList(List<CarComponent> cars);
}
