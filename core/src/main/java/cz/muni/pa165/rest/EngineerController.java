package cz.muni.pa165.rest;

import cz.muni.pa165.facade.EngineerFacade;
import cz.muni.pa165.generated.core.api.EngineerServiceApiDelegate;
import cz.muni.pa165.generated.core.model.EngineerCreateDto;
import cz.muni.pa165.generated.core.model.EngineerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EngineerController implements EngineerServiceApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(EngineerController.class);

    private final EngineerFacade engineerFacade;

    @Autowired
    public EngineerController(EngineerFacade engineerFacade) {
        this.engineerFacade = engineerFacade;
    }

    @Override
    public ResponseEntity<EngineerDto> createEngineer(EngineerCreateDto engineerCreateDto) {
        log.debug("createEngineer() called with createDao:" + engineerCreateDto);
        return new ResponseEntity<>(engineerFacade.createEngineer(engineerCreateDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteEngineer(Long id) {
        log.debug("deleteEngineer() called with id:" + id);
        engineerFacade.deleteEngineer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<EngineerDto> getEngineer(Long id) {
        log.debug("getEngineer() called with id:" + id);
        return new ResponseEntity<>(engineerFacade.getEngineer(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<EngineerDto>> getEngineers() {
        log.debug("getEngineers() called");
        return new ResponseEntity<>(engineerFacade.getEngineers(), HttpStatus.OK);
    }
}
