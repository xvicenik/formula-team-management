package cz.muni.pa165.rest;

import cz.muni.pa165.facade.DepartmentFacade;
import cz.muni.pa165.generated.core.api.DepartmentServiceApiDelegate;
import cz.muni.pa165.generated.core.model.DepartmentCreateDto;
import cz.muni.pa165.generated.core.model.DepartmentDto;
import cz.muni.pa165.generated.core.model.DepartmentUpdateDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Alžbeta Hajná
 */

@Component
public class DepartmentController implements DepartmentServiceApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

    private final DepartmentFacade departmentFacade;

    @Autowired
    public DepartmentController(DepartmentFacade departmentFacade) {
        this.departmentFacade = departmentFacade;
    }

    @Override
    public ResponseEntity<DepartmentDto> addEngineer(Long departmentId, Long engineerId) {
        log.debug("addEngineer() called");
        return new ResponseEntity<>(departmentFacade.addEngineer(departmentId, engineerId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DepartmentDto> createDepartment(DepartmentCreateDto departmentCreateDto) {
        log.debug("createDepartment() called");

        return new ResponseEntity<>(departmentFacade.createDepartment(departmentCreateDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteDepartment(Long id) {
        log.debug("deleteDepartment() called");

        departmentFacade.deleteDepartment(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DepartmentDto> getDepartment(Long id) {
        log.debug("getDepartment() called");

        return new ResponseEntity<>(departmentFacade.findById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DepartmentDto>> getDepartments() {
        log.debug("getDepartments() called");
        return new ResponseEntity<>(departmentFacade.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DepartmentDto> updateSpecialization(Long id, DepartmentUpdateDto departmentUpdateDto) {
        log.debug("updateSpecialization() called");
        return new ResponseEntity<>(departmentFacade.updateSpecialization(id, departmentUpdateDto), HttpStatus.OK);
    }

    private DepartmentDto findById(Long id) {
        return departmentFacade.findById(id);
    }
}
