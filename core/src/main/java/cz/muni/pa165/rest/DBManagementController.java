package cz.muni.pa165.rest;

import cz.muni.pa165.generated.core.api.DbManagementServiceApiDelegate;
import cz.muni.pa165.generated.core.model.SuccessfullyProcessed;
import cz.muni.pa165.service.DBManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author Michal Badin
 */
@Profile("dev")
@Component
public class DBManagementController implements DbManagementServiceApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(DBManagementController.class);

    private final DBManagementService dbManagementService;

    @Autowired
    public DBManagementController(DBManagementService dbManagementService) {
        this.dbManagementService = dbManagementService;
    }

    @Override
    public ResponseEntity<SuccessfullyProcessed> seedDB() {
        log.debug("seedDB() called");
        dbManagementService.seed();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<SuccessfullyProcessed> clearDB() {
        log.debug("clearDB() called");
        dbManagementService.clear();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
