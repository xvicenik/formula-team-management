package cz.muni.pa165.rest;

import cz.muni.pa165.facade.DriverFacade;
import cz.muni.pa165.generated.core.api.DriverServiceApiDelegate;
import cz.muni.pa165.generated.core.model.DriverCreateDto;
import cz.muni.pa165.generated.core.model.DriverDto;
import cz.muni.pa165.generated.core.model.DriverUpdateDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Michal Badin
 */
@Component
public class DriverController implements DriverServiceApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(DriverController.class);
    private final DriverFacade driverFacade;

    @Autowired
    public DriverController(DriverFacade driverFacade) {
        this.driverFacade = driverFacade;
    }

    @Override
    public ResponseEntity<Void> deleteDriver(Long id) {
        log.debug("deleteDrive() called");
        driverFacade.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DriverDto>> getAllTestDrivers() {
        log.debug("getAllTestDrivers() called");
        List<DriverDto> drivers = driverFacade.getAllTestDrivers();
        return new ResponseEntity<>(drivers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DriverDto> updateDriver(Long id, DriverUpdateDto driverUpdateDto) {
        log.debug("updateDriver() called");
        DriverDto driver = driverFacade.update(id, driverUpdateDto);
        return new ResponseEntity<>(driver, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DriverDto>> getAllDrivers() {
        log.debug("getAllDriver() called");
        List<DriverDto> drivers = driverFacade.getAllDrivers();
        return new ResponseEntity<>(drivers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DriverDto> getDriver(Long id) {
        log.debug("getDriver() called");
        DriverDto driver = driverFacade.findById(id);
        return new ResponseEntity<>(driver, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DriverDto> createDriver(DriverCreateDto driverCreateDto) {
        log.debug("createDriver() called");
        DriverDto driver = driverFacade.create(driverCreateDto);
        return new ResponseEntity<>(driver, HttpStatus.OK);
    }
}
