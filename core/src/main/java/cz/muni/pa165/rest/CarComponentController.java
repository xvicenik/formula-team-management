package cz.muni.pa165.rest;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.facade.CarComponentFacade;
import cz.muni.pa165.generated.core.api.CarComponentServiceApiDelegate;
import cz.muni.pa165.generated.core.model.CarComponentCreateDto;
import cz.muni.pa165.generated.core.model.CarComponentDto;
import cz.muni.pa165.generated.core.model.CarComponentType;
import cz.muni.pa165.generated.core.model.CarComponentUpdateDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CarComponentController implements CarComponentServiceApiDelegate {

    private static final Logger log = LoggerFactory.getLogger(CarComponentController.class);


    private final CarComponentFacade componentFacade;

    @Autowired
    public CarComponentController(CarComponentFacade componentFacade) {
        this.componentFacade = componentFacade;
    }


    @Override
    public ResponseEntity<CarComponentDto> getCarComponent(Long id) {
        log.debug("getCarComponent() called");

        return ResponseEntity.ok(componentFacade.findById(id));
    }

    public ResponseEntity<List<CarComponentDto>> getAllCarComponents() {
        log.debug("getAllCarComponents() called");

        return ResponseEntity.ok(componentFacade.findAll());
    }

    public ResponseEntity<List<CarComponentDto>> getCarComponentsByType(CarComponentType componentType) {
        log.debug("getCarComponentsByType() called");

        List<CarComponentDto> components = componentFacade.findAllByType(ComponentTypeEnum.fromValue(componentType.getValue()));
        return new ResponseEntity<>(components, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarComponentDto> createCarComponent(CarComponentCreateDto componentCreateDto) {
        log.debug("createCarComponent() called");

        return ResponseEntity.ok(componentFacade.create(componentCreateDto));
    }

    @Override
    public ResponseEntity<Void> deleteCarComponent(Long id) {
        log.debug("deleteCarComponent() called");

        componentFacade.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarComponentDto> updateCarComponent(Long id, CarComponentUpdateDto componentUpdateDto) {
        log.debug("updateCarComponent() called");

        return new ResponseEntity<>(componentFacade.update(id, componentUpdateDto), HttpStatus.OK);

    }

}
