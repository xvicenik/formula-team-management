package cz.muni.pa165.rest;

import cz.muni.pa165.facade.CarFacade;
import cz.muni.pa165.generated.core.api.CarServiceApiDelegate;
import cz.muni.pa165.generated.core.model.CarDto;
import cz.muni.pa165.generated.core.model.CarDtoPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CarController implements CarServiceApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(CarController.class);
    private final CarFacade carFacade;

    @Autowired
    public CarController(CarFacade carFacade) {
        this.carFacade = carFacade;
    }

    @Override
    public ResponseEntity<CarDto> createCar(List<Long> componentIds) {
        log.debug("createCar() called");
        return ResponseEntity.ok(carFacade.create(componentIds));
    }

    @Override
    public ResponseEntity<CarDto> setDriver(Long carId, Long driverId) {
        log.debug("setDriver() called");
        return new ResponseEntity<>(carFacade.setDriver(carId, driverId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> removeDriver(Long carId) {
        log.debug("removeDriver() called");
        carFacade.removeDriver(carId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarDto> getCar(Long id) {
        log.debug("getCar() called");
        return ResponseEntity.ok(carFacade.get(id));
    }

    @Override
    public ResponseEntity<Void> deleteCar(Long id) {
        log.debug("deleteCar() called");
        carFacade.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarDto> updateCar(Long id, List<Long> componentIds) {
        log.debug("updateCar() called");
        return new ResponseEntity<>(carFacade.update(id, componentIds), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarDtoPage> getAllCars(Integer page, Integer size) {
        log.debug("getAllCars() called");
        return new ResponseEntity<>(carFacade.getAllCars(page, size), HttpStatus.OK);
    }
}
