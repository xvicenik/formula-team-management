package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Department;
import cz.muni.pa165.data.model.Engineer;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
class DepartmentRepositoryTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EntityManager entityManager;

    private final Department department = new Department("dep1", new HashSet<>());
    private final Department emptyDepartment = new Department("dep2", new HashSet<>());
    private final Engineer engineer = new Engineer("John", "Doe", null);

    @BeforeEach
    void setUp() {
        department.addEngineer(engineer);
        entityManager.persist(engineer);
        entityManager.persist(department);
        entityManager.persist(emptyDepartment);
        entityManager.flush();
    }

    @Test
    void findExistingById() {
        Optional<Department> departmentFetched = departmentRepository.findById(department.getId());

        assertNotNull(departmentFetched);
        assertTrue(departmentFetched.isPresent());

        assertEquals(department, departmentFetched.get());
    }

    @Test
    void findNonExistingById() {
        Optional<Department> departmentFetched = departmentRepository.findById(Long.MAX_VALUE);

        assertNotNull(departmentFetched);
        assertTrue(departmentFetched.isEmpty());
    }

    @Test
    void findAll() {
        List<Department> departmentList = departmentRepository.findAll();

        assertNotNull(departmentList);
        assertEquals(List.of(department, emptyDepartment), departmentList);
    }


    @Test
    void delete() {
        departmentRepository.delete(department);

        entityManager.flush();

        assertEquals(1, departmentRepository.count());

        Department departmentFetched = entityManager.find(Department.class, department.getId());
        assertNull(departmentFetched);

        List<Department> departmentList = departmentRepository.findAll();
        assertNotNull(departmentList);
        assertEquals(List.of(emptyDepartment), departmentList);
    }

    @Test
    void deleteNonExisting() {
        Department departmentToDelete = new Department("dep3", new HashSet<>());
        departmentRepository.delete(departmentToDelete);

        Optional<Department> departmentFetched = departmentRepository.findById(departmentToDelete.getId());

        assertNotNull(departmentFetched);
        assertTrue(departmentFetched.isEmpty());

        List<Department> departmentList = departmentRepository.findAll();

        assertNotNull(departmentList);
        assertEquals(List.of(department, emptyDepartment), departmentList);
    }
}