package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Department;
import cz.muni.pa165.data.model.Engineer;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
class EngineerRepositoryTest {

    @Autowired
    private EngineerRepository engineerRepository;

    @Autowired
    private EntityManager entityManager;

    private final Department department = new Department("dep1", new HashSet<>());
    private final Engineer engineer = new Engineer("Samantha", "Lee", null);
    private final Engineer engineer2 = new Engineer("John", "Doe", department);

    @BeforeEach
    void setUp() {
        department.addEngineer(engineer2);
        entityManager.persist(engineer);
        entityManager.persist(department);
        entityManager.persist(engineer2);
        entityManager.flush();
    }

    @Test
    void findExistingById() {
        Optional<Engineer> engineerFetched = engineerRepository.findById(engineer.getId());

        assertNotNull(engineerFetched);
        assertTrue(engineerFetched.isPresent());

        assertEquals(engineer, engineerFetched.get());
    }

    @Test
    void findNonExistingById() {
        Optional<Engineer> engineerFetched = engineerRepository.findById(Long.MAX_VALUE);

        assertNotNull(engineerFetched);
        assertTrue(engineerFetched.isEmpty());
    }

    @Test
    void findAll() {
        List<Engineer> engineerList = engineerRepository.findAll();

        assertNotNull(engineerList);
        assertEquals(List.of(engineer, engineer2), engineerList);
    }

    @Test
    void delete() {
        engineerRepository.delete(engineer);

        entityManager.flush();

        assertEquals(1, engineerRepository.count());

        Engineer engineerFetched = entityManager.find(Engineer.class, engineer.getId());
        assertNull(engineerFetched);

        List<Engineer> engineerList = engineerRepository.findAll();
        assertNotNull(engineerList);
        assertEquals(List.of(engineer2), engineerList);
    }

    @Test
    void deleteNonExisting() {
        Engineer nonExistingEngineer = new Engineer();
        nonExistingEngineer.setId(999L);
        engineerRepository.delete(nonExistingEngineer);

        Optional<Engineer> nonExistingEngineerFetched = engineerRepository.findById(nonExistingEngineer.getId());

        assertNotNull(nonExistingEngineerFetched);
        assertTrue(nonExistingEngineerFetched.isEmpty());

        List<Engineer> engineerList = engineerRepository.findAll();

        assertNotNull(engineerList);
        assertEquals(List.of(engineer, engineer2), engineerList);
    }
}