package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.Driver;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
class DriverRepositoryTest {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private EntityManager entityManager;

    private Driver driverWithoutCar;
    private Driver driverWithCar;

    @BeforeEach
    void setUp() {
        driverWithoutCar = new Driver("jan", "novak", 175,
                LocalDate.of(1990, 1, 1), "czech", null, null);
        entityManager.persist(driverWithoutCar);

        var car = new Car();
        entityManager.persist(car);

        driverWithCar = new Driver("jan", "novak", 175,
                LocalDate.of(1990, 1, 1), "czech", car, null);
        entityManager.persist(driverWithCar);
        car.setDriver(driverWithCar);

        entityManager.flush();
    }

    @Test
    void findExistingById() {
        Optional<Driver> driverFetched = driverRepository.findById(driverWithoutCar.getId());

        assertNotNull(driverFetched);
        assertTrue(driverFetched.isPresent());

        assertEquals(driverWithoutCar, driverFetched.get());
    }

    @Test
    void findNonExistingById() {
        Optional<Driver> carComponentFetched = driverRepository.findById(Long.MAX_VALUE);

        assertNotNull(carComponentFetched);
        assertTrue(carComponentFetched.isEmpty());
    }

    @Test
    void findAllTestDrivers() {
        var testDrivers = driverRepository.findAllTestDrivers();

        assertNotNull(testDrivers);
        assertEquals(1, testDrivers.size());
    }

    @Test
    void delete() {
        driverRepository.delete(driverWithoutCar);

        Optional<Driver> driverFetched = driverRepository.findById(driverWithoutCar.getId());

        assertTrue(driverFetched.isEmpty());
    }

    @Test
    void deleteNonExisting() {
        var driverToDelete = new Driver();
        driverRepository.delete(driverToDelete);

        Optional<Driver> driverFetched = driverRepository.findById(driverToDelete.getId());

        assertNotNull(driverFetched);
        assertTrue(driverFetched.isEmpty());

        var driverList = driverRepository.findAll();

        assertNotNull(driverList);
        assertEquals(List.of(driverWithCar, driverWithoutCar), driverList);
    }
}
