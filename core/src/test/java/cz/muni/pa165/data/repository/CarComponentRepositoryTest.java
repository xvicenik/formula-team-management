package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
class CarComponentRepositoryTest {

    @Autowired
    private CarComponentRepository carComponentRepository;

    @Autowired
    private EntityManager entityManager;

    private CarComponent carComponent;
    private CarComponent carComponentWithoutCar;

    @BeforeEach
    void setUp() {
        Car car = new Car();
        entityManager.persist(car);

        carComponent = new CarComponent(
                ComponentTypeEnum.CHASSIS,
                180.0,
                "Dummy information",
                car
        );
        entityManager.persist(carComponent);
        car.setComponents(Set.of(carComponent));

        carComponentWithoutCar = new CarComponent(
                ComponentTypeEnum.ENGINE,
                180.0,
                "Dummy information",
                null
        );
        entityManager.persist(carComponentWithoutCar);

        entityManager.flush();
    }

    @Test
    void findExistingById() {
        Optional<CarComponent> carComponentFetched = carComponentRepository.findById(carComponent.getId());

        assertNotNull(carComponentFetched);
        assertTrue(carComponentFetched.isPresent());

        assertEquals(carComponent, carComponentFetched.get());
    }

    @Test
    void findNonExistingById() {
        Optional<CarComponent> carComponentFetched = carComponentRepository.findById(Long.MAX_VALUE);

        assertNotNull(carComponentFetched);
        assertTrue(carComponentFetched.isEmpty());
    }

    @Test
    void findAll() {
        List<CarComponent> carComponentList = carComponentRepository.findAll();

        assertNotNull(carComponentList);
        assertEquals(List.of(carComponent, carComponentWithoutCar), carComponentList);
    }

    @Test
    void findByType() {
        List<CarComponent> carComponentList = carComponentRepository.findByType(ComponentTypeEnum.ENGINE);

        assertNotNull(carComponentList);
        assertEquals(List.of(carComponentWithoutCar), carComponentList);
    }

    @Test
    void delete() {
        CarComponent carComponentToDelete = new CarComponent(ComponentTypeEnum.CHASSIS, 180.0, "Dummy information", null);
        entityManager.persist(carComponentToDelete);

        carComponentRepository.delete(carComponentToDelete);

        entityManager.flush();

        assertEquals(2, carComponentRepository.count());

        CarComponent carComponentFetched = entityManager.find(CarComponent.class, carComponentToDelete.getId());
        assertNull(carComponentFetched);

        List<CarComponent> carComponentList = carComponentRepository.findAll();
        assertNotNull(carComponentList);
        assertEquals(List.of(carComponent, carComponentWithoutCar), carComponentList);
    }

    @Test
    void deleteNonExisting() {
        CarComponent carComponentToDelete = new CarComponent();
        carComponentRepository.delete(carComponentToDelete);

        Optional<CarComponent> carComponentFetched = carComponentRepository.findById(carComponentToDelete.getId());

        assertNotNull(carComponentFetched);
        assertTrue(carComponentFetched.isEmpty());

        List<CarComponent> carComponentList = carComponentRepository.findAll();

        assertNotNull(carComponentList);
        assertEquals(List.of(carComponent, carComponentWithoutCar), carComponentList);
    }
}