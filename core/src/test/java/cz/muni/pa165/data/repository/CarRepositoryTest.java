package cz.muni.pa165.data.repository;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.model.Driver;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Michal Badin
 */
@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class CarRepositoryTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CarRepository carRepository;

    private Car car;

    @BeforeEach
    void setUp() {
        car = new Car();

        Driver driver = new Driver("John", "Doe", 180, LocalDate.of(1990, 1, 1), "Czech", car, new HashSet<>());

        CarComponent carComponent = new CarComponent(ComponentTypeEnum.CHASSIS, 180.0, "Dummy information", car);

        car.setDriver(driver);
        car.setComponents(Set.of(carComponent));

        entityManager.persist(car);
        entityManager.persist(carComponent);
        entityManager.persist(driver);
        entityManager.flush();
    }

    @Test
    void testSave() {
        Car carToSaved = new Car();

        Car saved = carRepository.save(carToSaved);

        assertNotNull(saved);
        Assertions.assertEquals(carToSaved, saved);
    }

    @Test
    void findByExistingId() {
        Optional<Car> carFetched = carRepository.findById(car.getId());

        assertNotNull(carFetched);
        assertTrue(carFetched.isPresent());

        assertEquals(car, carFetched.get());
    }

    @Test
    void findByNonExistingId() {
        Optional<Car> carFetched = carRepository.findById(Long.MAX_VALUE);

        assertNotNull(carFetched);
        assertTrue(carFetched.isEmpty());
    }

    @Test
    void delete() {
        Car carToDelete = new Car();
        entityManager.persist(carToDelete);

        carRepository.delete(carToDelete);

        Optional<Car> carFetched = carRepository.findById(carToDelete.getId());

        assertTrue(carFetched.isEmpty());
    }

    @Test
    void deleteNonExisting() {
        var carToDelete = new Car();
        carRepository.delete(carToDelete);

        Optional<Car> carFetched = carRepository.findById(carToDelete.getId());

        assertNotNull(carFetched);
        assertTrue(carFetched.isEmpty());

        var carList = carRepository.findAll();

        assertNotNull(carList);
        assertEquals(List.of(car), carList);
    }
}
