package cz.muni.pa165;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.*;
import cz.muni.pa165.generated.core.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "mvn verify / mvn test / mvn clean install".
 * Testing functionality of the module as a whole.
 */
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class CoreIT {

    private static final Logger log = LoggerFactory.getLogger(CoreIT.class);
    private final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testCreateCar() throws Exception {
        log.info("testCreateCar() running");

        CarDto carDto = new CarDto();
        String requestBody = mapper.writeValueAsString(carDto);

        String response = mockMvc.perform(
                        post("/car")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("response: {}", response);

        Car received = mapper.readValue(response, Car.class);
        Assertions.assertNotNull(received);
    }

    @Test
    void testCreateComponent() throws Exception {
        log.info("testCreateComponent() running");

        CarComponentDto componentDto = new CarComponentDto()
                .componentType(CarComponentType.CHASSIS)
                .weight(BigDecimal.valueOf(50.5))
                .information("lightweight front wing v2 (black)");
        String requestBody = mapper.writeValueAsString(componentDto);

        String response = mockMvc.perform(
                        post("/carComponent")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CarComponent received = mapper.readValue(response, CarComponent.class);
        Assertions.assertEquals(ComponentTypeEnum.CHASSIS, received.getComponentType());
        Assertions.assertEquals(50.5, received.getWeight());
        Assertions.assertEquals("lightweight front wing v2 (black)", received.getInformation());
    }

    @Test
    void testCreateComponentWrongData() throws Exception {
        log.info("testCreateComponentWrongData() running");

        CarComponentDto componentDto = new CarComponentDto()
                .componentType(CarComponentType.CHASSIS)
                .weight(BigDecimal.valueOf(-5))
                .information("lightweight front wing v2 (black)");
        String requestBody = mapper.writeValueAsString(componentDto);

        mockMvc.perform(
                        post("/carComponent")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void testCreateDriver() throws Exception {
        log.info("testCreateDriver running");

        DriverDto driverDto = new DriverDto()
                .name("John")
                .surname("Doe")
                .nationality("Italian")
                .height(189)
                .birthday(LocalDate.of(1990, 10, 1))
                .addCharacteristicsItem(Characteristic.AGGRESSIVENESS);
        String requestBody = mapper.writeValueAsString(driverDto);

        String response = mockMvc.perform(
                        post("/driver")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("response: {}", response);

        Driver received = mapper.readValue(response, Driver.class);
        Assertions.assertEquals("John", received.getName());
        Assertions.assertEquals("Doe", received.getSurname());
        Assertions.assertEquals("Italian", received.getNationality());
        Assertions.assertEquals(189, received.getHeight());
        Assertions.assertEquals(LocalDate.of(1990, 10, 1), received.getBirthday());
    }

    @Test
    void testCreateDriverWrongData() throws Exception {
        log.info("testCreateDriverWrongData running");

        DriverDto driverDto = new DriverDto()
                .name("John")
                .surname("Doe")
                .nationality("Italian")
                .height(50)
                .birthday(LocalDate.of(2999, 10, 1))
                .addCharacteristicsItem(Characteristic.AGGRESSIVENESS);
        String requestBody = mapper.writeValueAsString(driverDto);

        mockMvc.perform(
                        post("/driver")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void testCreateDepartment() throws Exception {
        log.info("testCreateDepartment() running");

        DepartmentDto departmentDto = new DepartmentDto().specialization("aero");
        String requestBody = mapper.writeValueAsString(departmentDto);

        String response = mockMvc.perform(
                        post("/department")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("response: {}", response);

        Department received = mapper.readValue(response, Department.class);
        Assertions.assertEquals("aero", received.getSpecialization());
    }

    @Test
    void testCreateDepartmentWrongData() throws Exception {
        log.info("testCreateDepartmentWrongData() running");

        DepartmentDto departmentDto = new DepartmentDto().specialization("");
        String requestBody = mapper.writeValueAsString(departmentDto);

        mockMvc.perform(
                        post("/department")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void testComplexScenario1() throws Exception {
        log.info("testComplexScenario1() running");

        Car car = loadEmptyCar();
        Driver driver = loadDriver();
        List<CarComponent> components = loadComponents();

        String carWithDriverResponse = mockMvc.perform(
                        put("/car/driver?carId=" + car.getId() + "&driverId=" + driver.getId())
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        Car carWithDriver = mapper.readValue(carWithDriverResponse, Car.class);
        car.setDriver(driver);
        Assertions.assertEquals(car, carWithDriver);

        car.setComponents(new HashSet<>(components));
        String carUpdateRequestQuery = components.stream().map(CarComponent::getId).map(String::valueOf).collect(Collectors.joining("&componentIds="));
        String carWithComponentsResponse = mockMvc.perform(
                        patch("/car/" + car.getId() + "?componentIds=" + carUpdateRequestQuery)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Car carWithComponents = mapper.readValue(carWithComponentsResponse, Car.class);
        Assertions.assertEquals(car, carWithComponents);

        mockMvc.perform(
                        delete("/car/driver?carId=" + car.getId()))
                .andExpect(status().isOk());

        String findCarDeletedDriver = mockMvc.perform(
                get("/car/" + car.getId())).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Car carDeletedDriver = mapper.readValue(findCarDeletedDriver, Car.class);
        Assertions.assertNull(carDeletedDriver.getDriver());

        mockMvc.perform(delete("/car/" + car.getId())).andExpect(status().isOk());

        mockMvc.perform(get("/car/" + car.getId())).andExpect(status().isNotFound());
    }

    @Test
    void testComplexScenario2() throws Exception {
        log.info("testComplexScenario1() running");

        Department department = loadEmptyDepartment();
        Engineer engineer = loadEngineer();

        String departmentWithEngineerResponse = mockMvc.perform(
                        put("/department/" + department.getId() + "?engineerId=" + engineer.getId())
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Department departmentWithEngineer = mapper.readValue(departmentWithEngineerResponse, Department.class);

        department.addEngineer(engineer);
        Assertions.assertEquals(1, departmentWithEngineer.getEngineers().size());
        Assertions.assertEquals(department, departmentWithEngineer);

        mockMvc.perform(delete("/department/" + department.getId())).andExpect(status().isOk());

        mockMvc.perform(get("/department/" + department.getId())).andExpect(status().isNotFound());
    }

    private Department loadEmptyDepartment() throws Exception {
        DepartmentDto departmentRequest = new DepartmentDto().specialization("aero").engineers(new ArrayList<>());
        String departmentRequestBody = mapper.writeValueAsString(departmentRequest);
        String departmentResponse = mockMvc.perform(post("/department")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(departmentRequestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Department received = mapper.readValue(departmentResponse, Department.class);
        received.setEngineers(new HashSet<>());
        return received;
    }

    private Engineer loadEngineer() throws Exception {
        EngineerDto engineerRequest = new EngineerDto().name("John").surname("Doe");
        String engineerRequestBody = mapper.writeValueAsString(engineerRequest);
        String engineerResponse = mockMvc.perform(post("/engineer")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(engineerRequestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        return mapper.readValue(engineerResponse, Engineer.class);
    }

    private Car loadEmptyCar() throws Exception {
        CarDto carRequest = new CarDto();
        String carRequestBody = mapper.writeValueAsString(carRequest);
        String carResponse = mockMvc.perform(post("/car")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(carRequestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        return mapper.readValue(carResponse, Car.class);
    }

    private Driver loadDriver() throws Exception {
        DriverDto driverRequest = new DriverDto()
                .name("John")
                .surname("Doe")
                .nationality("Italian")
                .height(189)
                .birthday(LocalDate.of(1990, 10, 1))
                .addCharacteristicsItem(Characteristic.AGGRESSIVENESS);
        String driverRequestBody = mapper.writeValueAsString(driverRequest);

        String driverResponse = mockMvc.perform(post("/driver")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(driverRequestBody))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        return mapper.readValue(driverResponse, Driver.class);
    }

    private List<CarComponent> loadComponents() throws Exception {
        CarComponentDto componentRequestChassis = new CarComponentDto()
                .componentType(CarComponentType.CHASSIS)
                .weight(BigDecimal.valueOf(50.5))
                .information("new aerodynamic chassis");
        CarComponentDto componentRequestEngine = new CarComponentDto()
                .componentType(CarComponentType.ENGINE)
                .weight(BigDecimal.valueOf(100))
                .information("honda engine v2 500HP");
        String componentRequestBodyChassis = mapper.writeValueAsString(componentRequestChassis);
        String componentRequestBodyEngine = mapper.writeValueAsString(componentRequestEngine);
        String componentResponseChassis = mockMvc.perform(post("/carComponent")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(componentRequestBodyChassis))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String componentResponseEngine = mockMvc.perform(post("/carComponent")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(componentRequestBodyEngine))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        CarComponent componentChassis = mapper.readValue(componentResponseChassis, CarComponent.class);
        CarComponent componentEngine = mapper.readValue(componentResponseEngine, CarComponent.class);
        return List.of(componentChassis, componentEngine);
    }

}

