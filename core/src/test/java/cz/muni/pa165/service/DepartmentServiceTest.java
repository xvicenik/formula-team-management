package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Department;
import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.data.repository.DepartmentRepository;
import cz.muni.pa165.data.repository.EngineerRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class DepartmentServiceTest {

    DepartmentRepository departmentRepository = Mockito.mock(DepartmentRepository.class);
    EngineerRepository engineerRepository = Mockito.mock(EngineerRepository.class);
    DepartmentService service = new DepartmentService(departmentRepository, engineerRepository);
    Department emptyDepartment = new Department("aero", new HashSet<>());


    @Test
    void create() {
        when(departmentRepository.save(emptyDepartment)).thenReturn(emptyDepartment);

        Department created = service.create(emptyDepartment);

        assertEquals(emptyDepartment, created);
    }

    @Test
    void addExistingEngineer() {
        Engineer engineer = new Engineer();
        Department expected = new Department("test", new HashSet<>());
        expected.addEngineer(engineer);

        when(engineerRepository.findById(engineer.getId())).thenReturn(java.util.Optional.of(engineer));
        when(departmentRepository.findById(emptyDepartment.getId())).thenReturn(java.util.Optional.of(emptyDepartment));
        when(departmentRepository.save(emptyDepartment)).thenReturn(expected);

        assertEquals(expected, service.addEngineer(emptyDepartment.getId(), engineer.getId()));
    }

    @Test
    void addEngineerAgain() {
        Engineer engineer = new Engineer();
        emptyDepartment.addEngineer(engineer);

        when(engineerRepository.findById(engineer.getId())).thenReturn(java.util.Optional.of(engineer));
        when(departmentRepository.findById(emptyDepartment.getId())).thenReturn(java.util.Optional.of(emptyDepartment));

        assertThrows(BadRequestException.class, () -> service.addEngineer(emptyDepartment.getId(), engineer.getId()));
    }

    @Test
    void addNonExistingEngineer() {
        when(engineerRepository.findById(1L)).thenReturn(Optional.empty());
        when(departmentRepository.findById(emptyDepartment.getId())).thenReturn(java.util.Optional.of(emptyDepartment));

        assertThrows(ResourceNotFoundException.class, () -> service.addEngineer(emptyDepartment.getId(), 1L));
    }

    @Test
    void updateSpecialization() {
        Department expected = new Department("Changed", new HashSet<>());

        when(departmentRepository.findById(emptyDepartment.getId())).thenReturn(java.util.Optional.of(emptyDepartment));
        when(departmentRepository.save(emptyDepartment)).thenReturn(expected);

        assertEquals(expected, service.updateSpecialization(emptyDepartment.getId(), expected));
    }

    @Test
    void updateSpecializationEmpty() {
        Department expected = new Department("", new HashSet<>());

        when(departmentRepository.findById(emptyDepartment.getId())).thenReturn(java.util.Optional.of(emptyDepartment));
        when(departmentRepository.save(emptyDepartment)).thenReturn(expected);

        assertEquals(expected, service.updateSpecialization(emptyDepartment.getId(), expected));
    }

}