package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.model.Driver;
import cz.muni.pa165.data.repository.CarComponentRepository;
import cz.muni.pa165.data.repository.CarRepository;
import cz.muni.pa165.data.repository.DriverRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static cz.muni.pa165.data.enums.ComponentTypeEnum.CHASSIS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Michal Badin
 */
public class CarServiceTest {

    private CarRepository carRepository;
    private DriverRepository driverRepository;
    private CarComponentRepository carComponentRepository;
    private WebClient.Builder webClientBuilder;

    private CarService carService;
    private Car car;

    @BeforeEach
    public void setUp() {
        carComponentRepository = mock(CarComponentRepository.class);
        carRepository = mock(CarRepository.class);
        driverRepository = mock(DriverRepository.class);
        webClientBuilder = WebClient.builder();
        carService = new CarService(carRepository, driverRepository, carComponentRepository, webClientBuilder);

        car = new Car();
        car.setId(1L);
    }

    @Test
    void testCreate() {
        Car createCar = new Car();
        createCar.setDriver(new Driver());
        createCar.setComponents(Set.of(new CarComponent()));

        when(carRepository.save(createCar)).thenReturn(createCar);

        assertEquals(carService.create(createCar), createCar);
        verify(carRepository, times(1)).save(createCar);
    }

    @Test
    void testDeleteExistingCar() {
        car.setComponents(Set.of(new CarComponent()));

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        carService.delete(car.getId());

        verify(carRepository, times(1)).delete(car);
    }

    @Test
    void testDeleteNonExistingCar() {
        Long carId = 1L;
        var exceptionToThrow = new ResourceNotFoundException(Car.class, carId);
        Optional<Car> carFromDb = Optional.empty();

        when(carRepository.findById(carId)).thenReturn(carFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.delete(carId);
        });
    }

    @Test
    void testUpdateExistingCar() {
        Car carForUpdate = new Car();
        CarComponent carComponent = new CarComponent();
        carComponent.setComponentType(CHASSIS);
        carComponent.setId(2L);
        carForUpdate.setComponents(Set.of(carComponent));
        carForUpdate.setId(car.getId());

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(carComponentRepository.findById(carComponent.getId())).thenReturn(Optional.of(carComponent));
        when(carRepository.save(car)).thenReturn(car);

        Car updatedCar = carService.update(car.getId(), List.of(carComponent.getId()));

        assertEquals(car, updatedCar);
        assertEquals(1, updatedCar.getComponents().size());
        assertTrue(updatedCar.getComponents().contains(carComponent));
        verify(carRepository, times(1)).save(car);
    }

    @Test
    void testUpdateNonExistingCar() {
        Long carId = 1L;
        var exceptionToThrow = new ResourceNotFoundException(Car.class, carId);
        List<Long> componentIds = Arrays.asList(2L, 3L);
        Optional<Car> carFromDb = Optional.empty();

        when(carRepository.findById(carId)).thenReturn(carFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.update(carId, componentIds);
        });
    }

    @Test
    void testUpdateExistingCarWithDuplicateComponent() {
        List<Long> componentIds = Arrays.asList(2L, 2L);
        var exceptionToThrow = new BadRequestException("Car can only have one component of each type");
        CarComponent component1 = new CarComponent();
        component1.setId(2L);

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(carComponentRepository.findById(component1.getId())).thenReturn(Optional.of(component1));

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.update(car.getId(), componentIds);
        });
    }

    @Test
    void testFindByIdExistingCar() {
        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));

        assertEquals(car, carService.findById(car.getId()));
    }

    @Test
    void testFindByIdNonExistingCar() {
        Long carId = 1L;
        var exceptionToThrow = new ResourceNotFoundException(Car.class, carId);
        Optional<Car> carFromDb = Optional.empty();

        when(carRepository.findById(carId)).thenReturn(carFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.findById(carId);
        });
    }

    @Test
    void testSetDriverExistingCar() {
        Driver driver = new Driver();
        driver.setId(2L);
        driver.setName("Driver");

        Car carWithDriver = new Car();
        carWithDriver.setDriver(driver);
        carWithDriver.setId(car.getId());

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));
        when(carRepository.save(car)).thenReturn(car);

        assertEquals(carWithDriver, carService.setDriver(car.getId(), driver.getId()));
        verify(carRepository, times(1)).save(car);
    }

    @Test
    void testSetDriverNonToExistingCar() {
        Long carId = 1L;
        var exceptionToThrow = new ResourceNotFoundException(Car.class, carId);
        Optional<Car> carFromDb = Optional.empty();
        Driver driver = new Driver();
        driver.setName("Driver");
        driver.setId(2L);

        when(carRepository.findById(carId)).thenReturn(carFromDb);
        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.setDriver(carId, driver.getId());
        });
    }

    @Test
    void testSetNonExistingDriverToExistingCar() {
        Long driverId = 2L;
        var exceptionToThrow = new ResourceNotFoundException(Driver.class, driverId);
        Optional<Driver> driverFromDb = Optional.empty();

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(driverId)).thenReturn(driverFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.setDriver(car.getId(), driverId);
        });
    }

    @Test
    void testSetDriverToCarWithDriverOfDifferentCar() {
        Car car1 = new Car();
        car1.setId(2L);
        Driver driver = new Driver();
        driver.setId(3L);
        driver.setName("Driver");
        driver.setCar(car1);

        var exceptionToThrow = new BadRequestException("Driver is already driver of a different car");

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.setDriver(car.getId(), driver.getId());
        });
    }

    @Test
    void testSetDriverToCarWithDriver() {
        Driver currDriver = new Driver();
        currDriver.setName("currDriver");
        currDriver.setId(2L);
        car.setDriver(currDriver);
        Driver newDriver = new Driver();
        newDriver.setName("newDriver");
        newDriver.setId(3L);
        var exceptionToThrow = new BadRequestException("Car already has a driver");

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(newDriver.getId())).thenReturn(Optional.of(newDriver));

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.setDriver(car.getId(), newDriver.getId());
        });
    }

    @Test
    void testRemoveDriverFromExistingCar() {
        Driver driver = new Driver();
        driver.setName("Driver");
        driver.setId(2L);
        car.setDriver(driver);
        driver.setCar(car);

        Car carWithOutDriver = new Car();
        carWithOutDriver.setId(car.getId());

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));
        when(driverRepository.save(driver)).thenReturn(driver);
        when(carRepository.save(car)).thenReturn(car);

        assertEquals(carWithOutDriver, carService.removeDriver(car.getId()));
        verify(carRepository, times(1)).save(car);
        verify(driverRepository, times(1)).save(driver);
    }

    @Test
    void testRemoveDriverFromNonExistingCar() {
        Long carId = 1L;
        Driver driver = new Driver();
        driver.setId(2L);
        driver.setName("Driver");
        var exceptionToThrow = new ResourceNotFoundException(Car.class, carId);
        Optional<Car> carFromDb = Optional.empty();

        when(carRepository.findById(car.getId())).thenReturn(carFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.removeDriver(carId);
        });
    }

    @Test
    void testRemoveNonExistingDriverFromExistingCar() {
        var exceptionToThrow = new BadRequestException(String.format("Car with id %s does not have a driver", car.getId()));

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.removeDriver(car.getId());
        });
    }

    @Test
    void testRemoveDriverFromCarWithoutDriver() {
        Driver driver = new Driver();
        driver.setId(2L);
        driver.setName("driver");
        car.setDriver(driver);
        var exceptionToThrow = new ResourceNotFoundException(Driver.class, driver.getId());
        Optional<Driver> driverFromDb = Optional.empty();

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(driverRepository.findById(driver.getId())).thenReturn(driverFromDb);

        assertThrows(exceptionToThrow.getClass(), () -> {
            carService.removeDriver(car.getId());
        });
    }

    @Test
    void findAll() {
        var car = new Car();
        Pageable pageable = PageRequest.of(0, 1);
        Page<Car> cars = new PageImpl<>(List.of(car), pageable, 1);

        when(carRepository.findAll(pageable)).thenReturn(cars);
        assertEquals(cars, carService.findAll(0, 1));
        verify(carRepository, times(1)).findAll(pageable);
    }

    @Test
    void findAllEmpty() {
        Pageable pageable = PageRequest.of(0, 1);
        Page<Car> cars = new PageImpl<>(List.of(), pageable, 0);

        when(carRepository.findAll(pageable)).thenReturn(cars);
        assertEquals(cars, carService.findAll(0, 1));
        verify(carRepository, times(1)).findAll(pageable);
    }

    @Test
    void findAllLast() {
        var car = new Car();
        Pageable pageable = PageRequest.of(1, 2);
        Page<Car> cars = new PageImpl<>(List.of(car), pageable, 1);

        when(carRepository.findAll(pageable)).thenReturn(cars);

        var result = carService.findAll(1, 2);
        assertEquals(cars, result);
        assertTrue(result.isLast());
        verify(carRepository, times(1)).findAll(pageable);
    }
}
