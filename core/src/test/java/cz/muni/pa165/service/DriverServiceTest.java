package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.CharacteristicsEnum;
import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.model.Driver;
import cz.muni.pa165.data.repository.DriverRepository;
import cz.muni.pa165.exceptions.BadRequestException;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DriverServiceTest {
    private DriverService driverService;
    private DriverRepository driverRepository;

    @BeforeEach
    void setUp() {
        driverRepository = mock(DriverRepository.class);
        driverService = new DriverService(driverRepository);
    }

    @Test
    void create() {
        var driver = createDriver();

        when(driverRepository.save(driver)).thenReturn(driver);

        assertEquals(driver, driverService.create(driver));
        verify(driverRepository, times(1)).save(driver);
    }

    @Test
    void updateExistingDriver() {
        var driver = createDriver();
        var driverForUpdate = new Driver("jan", "novak", 180,
                LocalDate.of(1990, 1, 1), "czech", null,
                new HashSet<>(List.of(CharacteristicsEnum.AGGRESSIVENESS)));

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));
        when(driverRepository.save(driver)).thenReturn(driver);

        assertEquals(driverForUpdate, driverService.update(driver.getId(), driverForUpdate));
        verify(driverRepository, times(1)).save(driver);
    }

    @Test
    void updateNonExistingDriver() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(driverRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            driverService.update(Long.MAX_VALUE, null);
        });
    }

    @Test
    void findExistingComponentById() {
        var driver = createDriver();

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));

        assertEquals(driver, driverService.findById(driver.getId()));
    }

    @Test
    void findNonExistingDriverById() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(driverRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            driverService.findById(Long.MAX_VALUE);
        });
    }

    @Test
    void findAllTestDrivers() {
        var driver = createDriver();
        var driver2 = new Driver("jana", "novakova", 175,
                LocalDate.of(1990, 1, 1), "czech", null, null);

        var testDrivers = List.of(driver, driver2);

        when(driverRepository.findAllTestDrivers()).thenReturn(testDrivers);

        assertEquals(testDrivers, driverService.findAllTestDrivers());
    }

    @Test
    void delete() {
        var driver = createDriver();

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));
        driverService.delete(driver.getId());
        verify(driverRepository, times(1)).delete(driver);
    }

    @Test
    void deleteNonExisting() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(driverRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            driverService.delete(Long.MAX_VALUE);
        });
    }

    @Test
    void deleteDriverWithCar() {
        var driver = createDriver();
        driver.setCar(new Car());

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));
        assertThrows(BadRequestException.class, () -> {
            driverService.delete(driver.getId());
        });
    }

    private Driver createDriver() {
        var driver = new Driver("jan", "novak", 175,
                LocalDate.of(1990, 1, 1), "czech", null, null);

        driver.setId(1L);
        return driver;
    }
}