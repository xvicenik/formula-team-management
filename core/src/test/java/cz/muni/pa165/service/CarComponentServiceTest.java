package cz.muni.pa165.service;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.data.model.Car;
import cz.muni.pa165.data.model.CarComponent;
import cz.muni.pa165.data.repository.CarComponentRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CarComponentServiceTest {
    private CarComponentService carComponentService;
    private CarComponentRepository carComponentRepository;
    private WebClient.Builder webClientBuilder;

    @BeforeEach
    void setUp() {
        carComponentRepository = mock(CarComponentRepository.class);
        webClientBuilder = WebClient.builder();
        carComponentService = new CarComponentService(carComponentRepository, webClientBuilder);
    }

    @Test
    void create() {
        CarComponent carComponent = createFilledComponent(new Car());

        when(carComponentRepository.save(carComponent)).thenReturn(carComponent);

        assertEquals(carComponent, carComponentService.create(carComponent));
        verify(carComponentRepository, times(1)).save(carComponent);
    }

    @Test
    void updateExistingComponent() {
        CarComponent carComponent = new CarComponent();
        CarComponent carComponentForUpdate = createFilledComponent(new Car());

        when(carComponentRepository.findById(carComponent.getId())).thenReturn(Optional.of(carComponent));
        when(carComponentRepository.save(carComponent)).thenReturn(carComponent);

        assertEquals(carComponentForUpdate, carComponentService.update(carComponent.getId(), carComponentForUpdate));
        verify(carComponentRepository, times(1)).save(carComponent);
    }

    @Test
    void updateNonExistingComponent() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(carComponentRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            carComponentService.update(Long.MAX_VALUE, null);
        });
    }

    @Test
    void findExistingComponentById() {
        CarComponent carComponent = createFilledComponent(new Car());

        when(carComponentRepository.findById(carComponent.getId())).thenReturn(Optional.of(carComponent));

        assertEquals(carComponent, carComponentService.findById(carComponent.getId()));
    }

    @Test
    void findNonExistingComponentById() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(carComponentRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            carComponentService.findById(Long.MAX_VALUE);
        });
    }

    @Test
    void findAll() {
        List<CarComponent> carComponentList = List.of(
                createFilledComponent(new Car()),
                createFilledComponent(new Car()),
                new CarComponent()
        );

        when(carComponentRepository.findAll()).thenReturn(carComponentList);

        assertEquals(carComponentList, carComponentService.findAll());
    }

    @Test
    void findAllByType() {
        List<CarComponent> carComponentList = List.of(
                createFilledComponent(new Car()),
                createFilledComponent(new Car())
        );

        when(carComponentRepository.findByType(ComponentTypeEnum.CHASSIS)).thenReturn(carComponentList);

        assertEquals(carComponentList, carComponentService.findAllByType(ComponentTypeEnum.CHASSIS));
    }

    @Test
    void delete() {
        CarComponent carComponent = createFilledComponent(new Car());

        when(carComponentRepository.findById(carComponent.getId())).thenReturn(Optional.of(carComponent));
        carComponentService.delete(carComponent.getId());
        verify(carComponentRepository, times(1)).delete(carComponent);
    }

    @Test
    void deleteNonExisting() {
        var exceptionToThrow = new ResourceNotFoundException(CarComponent.class, Long.MAX_VALUE);

        when(carComponentRepository.findById(Long.MAX_VALUE)).thenReturn(Optional.empty());

        assertThrows(exceptionToThrow.getClass(), () -> {
            carComponentService.delete(Long.MAX_VALUE);
        });
    }

    private CarComponent createFilledComponent(Car car) {
        return new CarComponent(
                ComponentTypeEnum.CHASSIS,
                180.0,
                "Dummy information",
                car
        );
    }
}