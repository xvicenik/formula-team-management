package cz.muni.pa165.service;

import cz.muni.pa165.data.model.Engineer;
import cz.muni.pa165.data.repository.EngineerRepository;
import cz.muni.pa165.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class EngineerServiceTest {

    EngineerRepository engineerRepository = Mockito.mock(EngineerRepository.class);
    EngineerService service = new EngineerService(engineerRepository);
    Engineer engineer = new Engineer();

    @Test
    void create() {
        when(engineerRepository.save(engineer)).thenReturn(engineer);

        Engineer created = service.create(engineer);

        assertEquals(engineer, created);
    }

    @Test
    void findExistingEngineerById() {
        when(engineerRepository.findById(engineer.getId())).thenReturn(Optional.of(engineer));
        assertEquals(engineer, service.findById(engineer.getId()));
    }

    @Test
    void findNonExistingEngineerById() {
        when(engineerRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.findById(1L));
    }
}


