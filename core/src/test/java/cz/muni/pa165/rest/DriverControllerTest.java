package cz.muni.pa165.rest;

import cz.muni.pa165.facade.DriverFacade;
import cz.muni.pa165.generated.core.model.Characteristic;
import cz.muni.pa165.generated.core.model.DriverUpdateDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DriverControllerTest {
    private static final Logger log = LoggerFactory.getLogger(CarComponentControllerTest.class);
    private final DriverFacade mockFacade = Mockito.mock(DriverFacade.class);
    private final DriverController controller = new DriverController(mockFacade);

    @Test
    void updateDriver() {
        log.debug("starting updateDriver()");

        DriverUpdateDto updateDto = new DriverUpdateDto().addCharacteristicsItem(Characteristic.AGGRESSIVENESS);

        controller.updateDriver(1L, updateDto);

        Mockito.verify(mockFacade, Mockito.times(1)).update(1L, updateDto);
    }

}
