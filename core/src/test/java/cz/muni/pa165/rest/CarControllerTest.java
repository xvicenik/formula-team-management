package cz.muni.pa165.rest;

import cz.muni.pa165.facade.CarFacade;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

class CarControllerTest {
    private static final Logger log = LoggerFactory.getLogger(CarControllerTest.class);

    private final CarFacade mockFacade = Mockito.mock(CarFacade.class);
    private final CarController controller = new CarController(mockFacade);

    @Test
    void testCreateCar() {
        log.debug("starting testCreateCar()");

        List<Long> components = new ArrayList<>();
        components.add(1L);

        controller.createCar(components);

        Mockito.verify(mockFacade, Mockito.times(1)).create(components);
    }

    @Test
    void testSetDriver() {
        log.debug("starting testSetDriver()");

        controller.setDriver(1L, 2L);

        Mockito.verify(mockFacade, Mockito.times(1)).setDriver(1L, 2L);
    }
}
