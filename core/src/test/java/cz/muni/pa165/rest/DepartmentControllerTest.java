package cz.muni.pa165.rest;

import cz.muni.pa165.facade.DepartmentFacade;
import cz.muni.pa165.generated.core.model.DepartmentUpdateDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DepartmentControllerTest {

    private static final Logger log = LoggerFactory.getLogger(DepartmentControllerTest.class);
    private final DepartmentFacade mockFacade = Mockito.mock(DepartmentFacade.class);
    private final DepartmentController controller = new DepartmentController(mockFacade);


    @Test
    void testAddEngineer() {
        log.debug("starting testAddEngineer()");

        controller.addEngineer(1L, 2L);

        Mockito.verify(mockFacade, Mockito.times(1)).addEngineer(1L, 2L);
    }

    @Test
    void testUpdateSpecialization() {
        log.debug("starting testUpdateSpecialization()");

        var updateDto = new DepartmentUpdateDto().specialization("test");
        controller.updateSpecialization(1L, updateDto);

        Mockito.verify(mockFacade, Mockito.times(1)).updateSpecialization(1L, updateDto);
    }


}
