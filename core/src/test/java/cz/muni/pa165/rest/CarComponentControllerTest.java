package cz.muni.pa165.rest;

import cz.muni.pa165.data.enums.ComponentTypeEnum;
import cz.muni.pa165.facade.CarComponentFacade;
import cz.muni.pa165.generated.core.model.CarComponentType;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CarComponentControllerTest {
    private static final Logger log = LoggerFactory.getLogger(CarComponentControllerTest.class);
    private final CarComponentFacade mockFacade = Mockito.mock(CarComponentFacade.class);
    private final CarComponentController carComponentController = new CarComponentController(mockFacade);

    @Test
    void testGetCarComponent() {
        log.debug("starting testGetCarComponent()");

        carComponentController.getCarComponent(1L);

        Mockito.verify(mockFacade, Mockito.times(1)).findById(1L);
    }

    @Test
    void testGetCarComponentsByType() {
        log.debug("starting testGetCarComponentsByType()");

        carComponentController.getCarComponentsByType(CarComponentType.CHASSIS);

        Mockito.verify(mockFacade, Mockito.times(1)).findAllByType(ComponentTypeEnum.CHASSIS);
    }
}

