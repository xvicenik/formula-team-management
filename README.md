# Formula team management

## Navigation
- [About Project](#about-project)
- [Build and run the app](#build-and-run-the-app)
- [Seed and clear DB](#seed-and-clear-db)
- [Build and run the app with Docker](#build-and-run-the-app-with-docker)
- [Collecting Metrics](#collecting-and-displaying-metrics)
- [Grafana](#grafana-setup)
- [Runnable scenario](#runnable-scenario)

## About Project

- **Name**: Formula One Team

- **Technologies**: Java 17, Spring, Maven, Tomcat

- **Developers**:

    - Alžbeta Hajná [xhajna](https://gitlab.fi.muni.cz/xhajna)
    - Jitka Viceníková [xvicenik](https://gitlab.fi.muni.cz/xvicenik)
    - Andrej Žabka [xzabka](https://gitlab.fi.muni.cz/xzabka)
    - Michal Badin [xbadin](https://gitlab.fi.muni.cz/xbadin)

- **Assigment**:
    - A Formula 1 team is in need to manage its participation to the world championship. The team has two cars
      participating to each race, each one driven by one driver, with other test drivers available to develop the car
      further. The manager of the team can set the driver for each car and manage a list of other (test) drivers that
      are under contract and he can also set as main drivers if unhappy about results. Each driver has a name, surname,
      nationality, and set of characteristics (e.g. driving on the wet, aggressiveness, etc..) that the manager can
      visualize. The car is composed of several components (engine, suspensions), that the different departments of the
      team (e.g. engine, aerodynamics) evolve during time. The manager can pick from a list of components and assemble
      the cars that will participate to the next race. Engineers from other departments can login to the system and put
      newer components available. The manager is notified that those components are available and that they can be used
      to improve the car.

- **Modules**:
  <br>Our application is divided into 4 separate spring boot modules, as shown on the class diagram

1) Core module:
   <br>Core module contains most of the functionality of the project assignment. It communicates with the other modules
   via their REST api's.
2) Notifications module:
   <br>Notifications module sends e-mail (or other) notifications to team members.
   <br>
   - Receives e-mails are determined based on provided e-mail during the creation of new applications and e-mails defined in 
   **application.properties** in **core** and **application modules**.
3) Visualisation module:
   <br>Visualization module provides a visualization of given JSON data in readable HTML (or other) format. Team manager
   will be able to visualize current state of both cars.
   - The result can be downloaded via Swagger UI or directly accessed in the module's folder **output-data**, which is created
   in case of missing.
4) Applications module:
   <br>Applications module allows people to send an application to this team. Manager can view these applications and
   hire new drivers for his team.

- **Diagrams**:

![IMAGE_DESCRIPTION](diagrams/class_diagram.png)
![IMAGE_DESCRIPTION](diagrams/use_case_diagram.png)

# Build and run the app

Build the app (all modules) by running

```bash
mvn clean install
```

in the project's parent directory, or build separate modules by running the same command inside the module's parent
directory. For example, to build notifications module:

```bash
cd notification
mvn clean install
```

Tests are run automatically during the build, but if you want to run them again, use:

```bash
mvn test
```

again, either in the parent directory to test all modules, or inside separate modules to only test one at a time.

To run a module, just run:

```bash
cd core
mvn
```

To see the running REST api's in action, use CURL or swagger UI:
http://localhost:8090/swagger-ui/index.html

Just note that each module runs on a different port by default, so care where you send your requests.<br>
**Default ports:**

- core module: 8090
- notifications module: 8083
- visualization module: 8082
- applications module: 8081

These ports can be overridden either in the application.properties file of each module, or by specifying the port
manually when running the "mvn" command to run the module.

## Seed and clear DB
DB is at the start of the app seeded with some Data, thus you don't have to do it manually. 
For users are available 2 endpoints:

- /seed - Seed DB with some Data
- /clear - Clear DB whenever during running

*Note: /seed will always seed DB with the same Data. Keep in mind, If you seed DB after startup, DB will contain the same data twice.*

## Build and run the app with Docker

For purpose of this build and run the installation of Docker/Podman
is required, steps to do so are not provided here.

*Note: If you are a win user using Docker Desktop, don't forget to run Docker Desktop first.*

### Building Docker images

To build individual docker images

```bash
docker build -t pa165-formula-team-management-<module> --target pa165-formula-team-management-<module> .
```

*Note: make sure you are in root directory of this project*

### Running Docker images

To run specific built docker image use command

```bash
docker run -p 8090:8090 -e DOCKER='1' pa165-formula-team-management-<module>
```

To avoid confusion the best way is to be consistent with ports listed above,
e.g. for running `application` image use:

```bash
docker run -p 8081:8081 -e DOCKER='1' pa165-formula-team-management-application
```

### Docker compose

To run all modules at once in one container, you can run (in root directory)

```bash
docker-compose up
```

*Note: in case you want to also rebuild images, use flag `--build`*
*Note: use flag `-d` for detached mode*

To stop container run

```bash
docker-compose down
```

## Collecting and displaying Metrics

To collect the metrics in an automated way we use Prometheus,
Grafana is used for displaying the most useful metrics.

Both services are running automatically in docker container together with other modules,
 when you run the docker-compose.<br>
You can check prometheus UI at http://localhost:9090.<br>
You can see grafana at http://localhost:3000

### Grafana setup

Credentials to log into Grafana are:
- user `admin`
- password `admin`

Two dashboards are imported automatically, but feel free to experiment and see different metrics.

## Runnable scenario

To showcase our system, we defined two scenarios:<br>
- Engineer, manager and their everyday business
- Applications module load test

### Engineer, manager and their everyday business
Our system is a management system for a formula 1 team. It is designed to help engineers and manager
to keep track of drivers, car components and current state of the cars. Naturally, this system will
not experience any high loads, so (in the core module) there is no reason to simulate user behaviour
with any kind of script. The best way to test the system is to manually try it.

#### 1. Become a manager of our F1 team (optional)
Navigate to ``/core/src/main/resources/application.properties`` file and add your e-mail address to the
``notification.receivers`` field. You can either delete the address that's already there, or add your own
separated with a comma(,). This means that you will get e-mail notifications same as the manager would.

#### 2. Run all modules
```bash
docker-compose up --build
```
Now, all the modules, including prometheus and grafana should be available through your web browser.
Feel free to play around with every module's swagger UI. Ports of all modules are listed above.<br>
Also feel free to monitor the system through grafana during all the following steps.

#### 3. Log in as an engineer and create a new component
- Visit swagger UI of core module at http://localhost:8090/swagger-ui/index.html. 
- Check, that without logging in, you are unable to perform any operations, except the /seed and /clear. 
- Log in with the authorize button and select the engineer scope (test_1). 
- Now, you are able to perform only the operations at the ``/carComponent`` endpoint.
- Create a new component that you just made with your team of engineers at your lab.
- Log out from the engineer scope.

If you followed step 1, you (as a manager) should also get an e-mail notification about 
new component being created. 

*Note: It is possible to time-out our team's e-mail address if you make too many new notifications (for example during the second scenario - load test). 
So if the Notifications module is not working, try to wait a few minutes, maybe another reviewer
was doing a load test at the same time. In real system we would not use a regular Gmail address for such job - there would be no timeouts.*

#### 4. Log in as an manager and create a visualization of a car
- Log in using a manager scope (test_5), same way as before.
- Now, you can perform all operations, except POST at /carComponent (only engineers can do that).
- Check, that the new component created by the engineer is really in the system.
- Play around with the other endpoints, for example try to change the driver in one of the cars. 
(If you cleared the database before, seed it so you don't have to create everything manually)
- Now call the GET operation on a certain car, this operation returns the car as a JSON directly in the response,
but also calls the Visualization module with that car.
- Check ``/visualization/output-data`` and you should be able to see a PDF visualization of the car you chose.
- Hopefully you chose the right components and drivers for your cars. Good luck in the next race.

This scenario showcased the functionality of the Core module and it's communication with Notification and Visualization modules.

### Applications module load test:
This scenario showcases a situation, where lots of people are sending applications to this F1 team.<br>
At the same time, some admin (someone from HR for example) is randomly accepting/rejecting these applications.<br>

Before running this scenario make sure you have installed ``python`` and ``locust`` module, which can be installed with<br>
``pip install locust``

#### 1. Run all modules

```bash
docker-compose up
```

#### 2. Run script

```bash
cd scenario-locust
locust -f scenario.py --host http://localhost:8081 --users 2
```
*Note: Script can be run without arguments, but then you have them fill up in the next step.*

#### 3. Open in web browser

```bash
http://localhost:8089/
```
Adjust ``Number of users`` and ``Spawn rate`` to test different scenarios.
<br>*Note: At least 2 users are required  to simulate ``Admin`` and ``User`` simultaneously*
<br>*Note: !Do not change Host!*

#### 4. Collecting and displaying Metrics
After started swarming in web browser, locust provides several pages to display various data.
Feel free to explore them, but two main pages are ``Statistics`` and ``Charts``

If you want to display more complex metrics, do not hesitate to use:
- ``Grafana`` http://localhost:9090
- ``Prometheus`` http://localhost:3000

#### 5. Clear Data - optional
Don't forget to clear your Application DB via ``/clear`` end point. 