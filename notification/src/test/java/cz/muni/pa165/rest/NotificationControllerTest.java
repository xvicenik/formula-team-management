package cz.muni.pa165.rest;

import cz.muni.pa165.facade.NotificationFacade;
import cz.muni.pa165.generated.model.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NotificationControllerTest {
    private static final Logger log = LoggerFactory.getLogger(NotificationControllerTest.class);

    private final NotificationFacade mockFacade = Mockito.mock(NotificationFacade.class);
    private final NotificationController controller = new NotificationController(mockFacade);

    @Test
    void testNotifyNewComponent() {
        log.debug("testNotifyNewComponent() running");

        var dto = new CarComponentNotificationDto()
                .carComponent(new CarComponentDto())
                .receivers(List.of("aaa@aaa.com"));

        controller.notifyNewComponent(dto);
        Mockito.verify(mockFacade, Mockito.times(1)).notifyNewComponent(dto);
    }

    @Test
    void testNotifyNewApplication() {
        log.debug("testNotifyNewApplication() running");

        var dto = new ApplicationNotificationDto()
                .application(new ApplicationDto())
                .receivers(List.of("aaa@aaa.com"));

        controller.notifyNewApplication(dto);
        Mockito.verify(mockFacade, Mockito.times(1)).notifyNewApplication(dto);
    }

    @Test
    void testNotifyApplicationStatusChange() {
        log.debug("testNotifyApplicationStatusChange() running");

        var dto = new ApplicationNotificationDto()
                .application(new ApplicationDto())
                .receivers(List.of("aaa@aaa.com"));

        controller.notifyApplicationStatusChange(dto);
        Mockito.verify(mockFacade, Mockito.times(1)).notifyApplicationStatusChange(dto);
    }
}
