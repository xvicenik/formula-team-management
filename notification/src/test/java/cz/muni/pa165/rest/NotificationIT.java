package cz.muni.pa165.rest;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "mvn verify / mvn test / mvn clean install".
 * No need to test all the rest controllers logic here, just test that the endpoints are working
 */
@SpringBootTest
@AutoConfigureMockMvc
class NotificationIT {

    private static final Logger log = LoggerFactory.getLogger(NotificationIT.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testNotificationModuleResponse() throws Exception {
        log.info("testNotificationModuleResponse() running");

        String testRequestContent = "{\"message\": \"string\",\"receivers\": [\"formula.team.management@gmail.com\"],\"subject\": \"string\"}";

        String response = mockMvc.perform(
                        post("/notification")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(testRequestContent))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("response: {}", response);

        String expectedResponse = "{\"message\":\"string\",\"subject\":\"string\",\"receivers\":[\"formula.team.management@gmail.com\"],\"sender\":\"formula.team.management@gmail.com\"}";

        assertEquals(expectedResponse, response);
    }

    @Test
    void testNotificationModuleNoRecipient() throws Exception {
        log.info("testNotificationModuleResponse() running");

        String testRequestContent = "{\"message\": \"string\",\"receivers\": [],\"subject\": \"string\"}";

        mockMvc.perform(
                post("/notification")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(testRequestContent))
                .andExpect(status().isInternalServerError());
    }
}