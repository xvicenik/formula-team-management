package cz.muni.pa165.facade;

import cz.muni.pa165.generated.model.ApplicationNotificationDto;
import cz.muni.pa165.generated.model.CarComponentNotificationDto;
import cz.muni.pa165.generated.model.ConfirmationDto;
import cz.muni.pa165.generated.model.NotificationDto;
import cz.muni.pa165.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationFacade {

    private final NotificationService notificationService;

    @Autowired
    public NotificationFacade(NotificationService notificationService) {

        this.notificationService = notificationService;
    }

    public ConfirmationDto notify(NotificationDto notificationDto) {
        return notificationService.notify(notificationDto);
    }

    public ConfirmationDto notifyNewComponent(CarComponentNotificationDto carComponentNotificationDto) {
        return notificationService.notifyNewComponent(carComponentNotificationDto);
    }

    public ConfirmationDto notifyNewApplication(ApplicationNotificationDto applicationDto) {
        return notificationService.notifyNewApplication(applicationDto);
    }

    public ConfirmationDto notifyApplicationStatusChange(ApplicationNotificationDto applicationDto) {
        return notificationService.notifyApplicationStatusChange(applicationDto);

    }
}
