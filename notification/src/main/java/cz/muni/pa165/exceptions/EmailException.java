package cz.muni.pa165.exceptions;

/**
 * @author Michal Badin
 */
public class EmailException extends RuntimeException {
    public EmailException(String message, Throwable cause) {
        super(message, cause);
    }
}
