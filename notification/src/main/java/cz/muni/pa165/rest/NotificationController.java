package cz.muni.pa165.rest;

import cz.muni.pa165.facade.NotificationFacade;
import cz.muni.pa165.generated.api.NotificationServiceApiDelegate;
import cz.muni.pa165.generated.model.ApplicationNotificationDto;
import cz.muni.pa165.generated.model.CarComponentNotificationDto;
import cz.muni.pa165.generated.model.ConfirmationDto;
import cz.muni.pa165.generated.model.NotificationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author Alžbeta Hajná
 */

@Component
public class NotificationController implements NotificationServiceApiDelegate {

    private static final Logger log = LoggerFactory.getLogger(NotificationController.class);
    private final NotificationFacade notificationFacade;

    @Autowired
    public NotificationController(NotificationFacade notificationFacade) {
        this.notificationFacade = notificationFacade;
    }

    @Override
    public ResponseEntity<ConfirmationDto> notify(NotificationDto notificationDto) {
        log.debug("notify() called");

        return new ResponseEntity<>(notificationFacade.notify(notificationDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ConfirmationDto> notifyNewComponent(CarComponentNotificationDto carComponentNotificationDto) {
        log.debug("notifyComponentChange() called");

        return new ResponseEntity<>(notificationFacade.notifyNewComponent(carComponentNotificationDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ConfirmationDto> notifyNewApplication(ApplicationNotificationDto applicationDto) {
        log.debug("notifyNewApplication() called");

        return new ResponseEntity<>(notificationFacade.notifyNewApplication(applicationDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ConfirmationDto> notifyApplicationStatusChange(ApplicationNotificationDto applicationDto) {
        log.debug("notifyApplicationStatusChange() called");

        return new ResponseEntity<>(notificationFacade.notifyApplicationStatusChange(applicationDto), HttpStatus.OK);
    }
}
