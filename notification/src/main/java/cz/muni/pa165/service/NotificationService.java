package cz.muni.pa165.service;

import cz.muni.pa165.exceptions.EmailException;
import cz.muni.pa165.generated.model.*;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class NotificationService {
    private static final String EMAIL = "formula.team.management@gmail.com";
    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);
    private final JavaMailSender emailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public NotificationService(JavaMailSender emailSender, TemplateEngine templateEngine) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
    }

    public ConfirmationDto notify(NotificationDto notificationDto) {
        return sendEmail(notificationDto.getSubject(), notificationDto.getMessage(), notificationDto.getReceivers());
    }

    public ConfirmationDto notifyNewComponent(CarComponentNotificationDto carComponentNotificationDto) {
        var c = carComponentNotificationDto.getCarComponent();
        var subject = "New component: " + c.getInformation();

        Context ctx = new Context();
        ctx.setVariable("id", c.getId());
        ctx.setVariable("type", c.getComponentType());
        ctx.setVariable("info", c.getInformation());
        ctx.setVariable("weight", c.getWeight());

        var htmlContent = templateEngine.process("new-component-email", ctx);

        return sendEmail(subject, htmlContent, carComponentNotificationDto.getReceivers());
    }

    public ConfirmationDto notifyNewApplication(ApplicationNotificationDto applicationNotificationDto) {
        var a = applicationNotificationDto.getApplication();
        var subject = "New application: " + getFullName(a);

        Context ctx = new Context();
        ctx.setVariable("id", a.getId());
        ctx.setVariable("name", a.getName());
        ctx.setVariable("surname", a.getSurname());
        ctx.setVariable("birthday", a.getBirthday().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        ctx.setVariable("email", a.getEmail());
        ctx.setVariable("fillingOutDate", a.getFillingOutDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        var htmlContent = templateEngine.process("new-application-email", ctx);

        return sendEmail(subject, htmlContent, applicationNotificationDto.getReceivers());
    }

    public ConfirmationDto notifyApplicationStatusChange(ApplicationNotificationDto applicationNotificationDto) {
        var a = applicationNotificationDto.getApplication();
        var subject = "Application for Formula Driver Position";
        var htmlContent = "";

        Context ctx = new Context();
        ctx.setVariable("fullName", getFullName(a));
        ctx.setVariable("today", LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        if (a.getStatus() == ApplicationStatus.ACCEPTED) {
            htmlContent = templateEngine.process("accepted-email", ctx);
        } else {
            htmlContent = templateEngine.process("rejected-email", ctx);
        }

        return sendEmail(subject, htmlContent, applicationNotificationDto.getReceivers());
    }

    private ConfirmationDto sendEmail(String subject, String text, List<String> receivers) {
        MimeMessage message = emailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
            helper.setFrom(EMAIL);
            helper.setTo(receivers.toArray(String[]::new));
            helper.setSubject(subject);
            helper.setText(text, true);

            emailSender.send(message);

            return new ConfirmationDto()
                    .sender(EMAIL)
                    .receivers(receivers)
                    .subject(subject)
                    .message(text);
        } catch (MessagingException e) {
            throw new EmailException("Failed to send email", e);
        }
    }

    private String getFullName(ApplicationDto applicationDto) {
        return applicationDto.getName() + " " + applicationDto.getSurname();
    }
}
